package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.PasswordRestoreUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PasswordRestoreViewModel(val passwordRestoreUseCase: PasswordRestoreUseCase) : ViewModel() {

    val passResetApiAnswer = MutableLiveData<OutcomeUi<Unit>>()
    val verifyPhoneApiCode = MutableLiveData<OutcomeUi<Unit>>()


    fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = viewModelScope.launch(Dispatchers.IO) {
        passwordRestoreUseCase.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from).collect {
            verifyPhoneApiCode.postValue(it)
        }
    }


    fun passReset(
        sharedPrefsUtils: SharedPrefsUtils,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        passwordRestoreUseCase.passReset(
            sharedPrefsUtils = sharedPrefsUtils,
            phone_number = phone_number,
            password1 = password1,
            password2 = password2,
            otp = otp
        ).collect {
            passResetApiAnswer.postValue(it)
        }
    }


}