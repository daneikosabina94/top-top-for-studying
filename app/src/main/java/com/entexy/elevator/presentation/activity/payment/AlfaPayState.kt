package com.entexy.elevator.presentation.activity.payment

enum class AlfaPayState {
    Success,
    Error,
    Expired
}