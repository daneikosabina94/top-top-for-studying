package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.api.profile.Profile
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.ProfileUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(val profileUseCase: ProfileUseCase) : ViewModel() {

    val mutableLiveData = MutableLiveData<OutcomeUi<Profile>>()
    val apiLogoutCode = MutableLiveData<OutcomeUi<Unit>>()

    fun getProfile(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        profileUseCase.getProfile(sharedPrefsUtils = sharedPrefsUtils).collect {
            mutableLiveData.postValue(it)
        }
    }

    fun logout(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        profileUseCase.logout(sharedPrefsUtils = sharedPrefsUtils).collect {
            apiLogoutCode.postValue(it)
        }
    }


}