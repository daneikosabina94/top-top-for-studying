package com.entexy.elevator.presentation.fragments.registration

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.*
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.PasswordRestoreViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class PasswordRestoreFragment : Fragment() {

    private lateinit var binding: FragmentPassRestoreBinding
    private val viewModel by viewModel<PasswordRestoreViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPassRestoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPrefsUtils = SharedPrefsUtils(requireContext())

        if (arguments == null) {
            binding.pass1Title.visibility = View.GONE
            binding.pass1Value.visibility = View.GONE
            binding.pass2Title.visibility = View.GONE
            binding.pass2Value.visibility = View.GONE
//            binding.checkBoxContainer.visibility = View.GONE
            binding.buttonRegistration.setOnClickListener {
                if (binding.telValue.text?.isEmpty() == true) return@setOnClickListener
                viewModel.verifyPhone(
                    sharedPrefsUtils = sharedPrefsUtils,
                    phone_number = binding.telValue.text.toString(),
                    from = "restore"
                )
            }
            viewModel.verifyPhoneApiCode.observe(viewLifecycleOwner) {
                when (it) {
                    is OutcomeUi.Progress -> binding.progressBarRestorePass.visibility = View.VISIBLE
                    is OutcomeUi.Failure -> {
                        binding.progressBarRestorePass.visibility = View.GONE
                        if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                    }

                    else -> {
                        sharedPrefsUtils.setUserPhone(binding.telValue.text.toString())
                        lifecycleScope.launchWhenResumed {
                            findNavController().navigate(
                                PasswordRestoreFragmentDirections.actionPasswordRestoreFragmentToTelephoneConfirmFragment(
                                    binding.telValue.text.toString(),
                                    true
                                )
                            )
                        }

                        binding.progressBarRestorePass.visibility = View.GONE
                    }
                }
            }
        } else {
            val otp = requireArguments().getString("otp", "")
            val tel = sharedPrefsUtils.getUserPhone()
            binding.telTitle.visibility = View.GONE
            binding.telValue.visibility = View.GONE
            binding.buttonRegistration.setOnClickListener {
                if (binding.pass1Value.text.toString().isNotBlank() && binding.pass2Value.text.toString().isNotBlank()) {
                    if (binding.pass1Value.text.toString() == binding.pass2Value.text.toString()) {
                        if (binding.pass1Value.text.toString().length > 7) {
                            viewModel.passReset(
                                sharedPrefsUtils = sharedPrefsUtils,
                                phone_number = tel,
                                password1 = binding.pass1Value.text.toString(),
                                password2 = binding.pass2Value.text.toString(),
                                otp = otp
                            )
                        } else Toast.makeText(requireContext(), getString(R.string.error_password_symbols), Toast.LENGTH_LONG).show()
                    } else Toast.makeText(requireContext(), getString(R.string.error_fields_dont_match), Toast.LENGTH_LONG).show()

                } else Toast.makeText(requireContext(), getString(R.string.error_empty_fields), Toast.LENGTH_LONG).show()
            }
            viewModel.passResetApiAnswer.observe(viewLifecycleOwner) {
                when (it) {
                    is OutcomeUi.Progress -> binding.progressBarRestorePass.visibility = View.VISIBLE
                    is OutcomeUi.Failure -> {
                        binding.progressBarRestorePass.visibility = View.GONE
                        if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                    }

                    else -> {
                        lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.loginFragment) }
                        binding.progressBarRestorePass.visibility = View.GONE

                    }
                }
            }
        }
        binding.superRootPasswordRestore.setOnClickListener { closeKeyBoard(it) }
        textFieldsBehavior()
    }

    private fun textFieldsBehavior() {
        binding.telValue.addTextChangedListener { checkTextEmptiness() }
        binding.pass1Value.addTextChangedListener { checkTextEmptiness() }
        binding.pass2Value.addTextChangedListener { checkTextEmptiness() }
    }

    private fun checkTextEmptiness() {
        if (arguments == null) {
            if (binding.telValue.text?.isNotEmpty() == true)
                binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            else binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
        } else {
            if (binding.pass1Value.text?.isNotEmpty() == true && binding.pass2Value.text?.isNotEmpty() == true)
                binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            else binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
        }
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


}