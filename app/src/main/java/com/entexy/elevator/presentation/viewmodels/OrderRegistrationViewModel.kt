package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.api.Order
import com.entexy.elevator.data.api.payment.binding.create.response.CreateBindingResponse
import com.entexy.elevator.data.api.payment.binding.verify.response.VerifyBindingResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.CreateBindingUseCase
import com.entexy.elevator.domain.uscases.GetAllBindingsUseCase
import com.entexy.elevator.domain.uscases.OrderRegistrationUseCase
import com.entexy.elevator.domain.uscases.VerifyBindingUseCase
import com.entexy.elevator.presentation.fragments.order.adapters.item.CardItem
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrderRegistrationViewModel(
    val orderRegistrationUseCase: OrderRegistrationUseCase,
    val createBindingUseCase: CreateBindingUseCase,
    val verifyBindingUseCase: VerifyBindingUseCase,
    val getAllBindingsUseCase: GetAllBindingsUseCase,
) : ViewModel() {

    val orders = MutableLiveData<OutcomeUi<Order>>()
    val apiAnswer = MutableLiveData<OutcomeUi<Unit>>()
    val confirmOrderApiCode = MutableLiveData<OutcomeUi<Unit>>()
    val cancelOrderApiCode = MutableLiveData<OutcomeUi<Unit>>()
    private val _createBindingStatus = MutableLiveData<OutcomeUi<CreateBindingResponse>>()
    val createBindingStatus: LiveData<OutcomeUi<CreateBindingResponse>> = _createBindingStatus
    private val _verifyBindingStatus = MutableLiveData<OutcomeUi<VerifyBindingResponse>>()
    val verifyBindingStatus: LiveData<OutcomeUi<VerifyBindingResponse>> = _verifyBindingStatus
    private val _getAllBindingsStatus = MutableLiveData<OutcomeUi<List<CardItem>>>()
    val getAllBindingsStatus: LiveData<OutcomeUi<List<CardItem>>> = _getAllBindingsStatus

    fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String,
        address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int,
        payment_card: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        orderRegistrationUseCase.postOrder(
            sharedPrefsUtils = sharedPrefsUtils,
            photosByteArrayList = photosByteArrayList,
            city = city,
            address = address,
            district = district,
            address_latitude = address_latitude,
            address_longitude = address_longitude,
            entrance = entrance,
            floor = floor,
            is_there_an_elevator = is_there_an_elevator,
            title = title,
            items_count = items_count,
            lifts_count = lifts_count,
            comment = comment,
            bring_to_floor = bring_to_floor,
            dimensions = dimensions,
            weight = weight,
            price = price,
            predicted_movers_count = predicted_movers_count,
            payment_card = payment_card
        ).collect {
            apiAnswer.postValue(it)
        }
    }

    fun createBinding(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        createBindingUseCase.createBinding(sharedPrefsUtils).collect {
            _createBindingStatus.postValue(it)
        }
    }

    fun verifyBinding(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        verifyBindingUseCase.verifyBinding(sharedPrefsUtils).collect {
            _verifyBindingStatus.postValue(it)
        }
    }

    fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        getAllBindingsUseCase.getAllBindings(sharedPrefsUtils).collect {
            _getAllBindingsStatus.postValue(it)
        }
    }

    fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = viewModelScope.launch(Dispatchers.IO) {
        orderRegistrationUseCase.getOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId).collect {
            orders.postValue(it)
        }
    }

    fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = viewModelScope.launch(Dispatchers.IO) {
        orderRegistrationUseCase.confirmOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId).collect {
            confirmOrderApiCode.postValue(it)
        }
    }

    fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        orderRegistrationUseCase.cancelOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId, isNeededPayment).collect {
            println("36ss 7")
            cancelOrderApiCode.postValue(it)
        }
    }
}
