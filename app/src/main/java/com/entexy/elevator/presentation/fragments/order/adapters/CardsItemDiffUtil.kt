package com.entexy.elevator.presentation.fragments.order.adapters

import androidx.recyclerview.widget.DiffUtil
import com.entexy.elevator.presentation.fragments.order.adapters.item.CardItem

class CardsItemDiffUtil : DiffUtil.ItemCallback<CardItem>() {
    override fun areItemsTheSame(oldItem: CardItem, newItem: CardItem) = false
    override fun areContentsTheSame(oldItem: CardItem, newItem: CardItem): Boolean = false
}