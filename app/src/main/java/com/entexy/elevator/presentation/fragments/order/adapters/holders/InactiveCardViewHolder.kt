package com.entexy.elevator.presentation.fragments.order.adapters.holders

import androidx.recyclerview.widget.RecyclerView
import com.entexy.elevator.databinding.RecyclerItemCardInactiveBinding

class InactiveCardViewHolder(val binding: RecyclerItemCardInactiveBinding) : RecyclerView.ViewHolder(binding.root)