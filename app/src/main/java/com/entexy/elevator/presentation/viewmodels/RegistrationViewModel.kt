package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.RegistrationUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegistrationViewModel(val registrationUseCase: RegistrationUseCase) : ViewModel() {

    val accessAndRefreshToken = MutableLiveData<OutcomeUi<MutableMap<String, String>>>()
    val verifyPhoneApiCode = MutableLiveData<OutcomeUi<Unit>>()


    fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        registrationUseCase.register(
            sharedPrefsUtils = sharedPrefsUtils,
            first_name = first_name,
            phone_number = phone_number,
            password1 = password1,
            password2 = password2,
            otp = otp
        ).collect {
            accessAndRefreshToken.postValue(it)
        }
    }

    fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = viewModelScope.launch(Dispatchers.IO) {
        registrationUseCase.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from).collect {
            verifyPhoneApiCode.postValue(it)
        }
    }
}