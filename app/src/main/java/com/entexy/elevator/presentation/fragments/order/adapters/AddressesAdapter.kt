package com.entexy.elevator.presentation.fragments.order.adapters

import android.location.Address
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.entexy.elevator.databinding.ItemAddressHolderBinding
import com.entexy.elevator.presentation.fragments.order.adapters.callbacks.OnAddressClickCallback

class AddressesAdapter(private val onItemClickCallback: OnAddressClickCallback) :
    RecyclerView.Adapter<AddressesAdapter.AddressViewHolder>() {
    private val items: MutableList<String> = mutableListOf()

    inner class AddressViewHolder(val binding: ItemAddressHolderBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder =
        AddressViewHolder(ItemAddressHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) = with(holder.binding) {
        val item = items[position]
        tvAddress.text = item
        tvAddress.setOnClickListener {
            onItemClickCallback.onAddressClick(tvAddress.text.toString())
        }
    }

    fun updateList(list: MutableList<Address>?) {
        items.clear()
        list?.let {
            it.forEach { address ->
                items.add(address.getAddressLine(0).toString())
            }
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size
}