package com.entexy.elevator.presentation.fragments.registration

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentLoginBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.LoginViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var navController: NavController
    private val viewModel by viewModel<LoginViewModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPrefsUtils = SharedPrefsUtils(requireContext())
        navController = findNavController()

        if (!sharedPrefsUtils.getRemember() && sharedPrefsUtils.getAccessToken().isNotEmpty()) {
            val currentBavBackStackEntry = navController.currentBackStackEntry
            findNavController().backQueue.clear()
            if (currentBavBackStackEntry != null) navController.backQueue.add(currentBavBackStackEntry)
        }

        requireActivity().onBackPressedDispatcher.addCallback {
            try {
                requireActivity().finish()
            } catch (ex: IllegalStateException) {
                navController.navigateUp()
                navController.popBackStack()
            }
        }

        binding.regisrtration.setOnClickListener {
            if (navController.currentDestination?.id == R.id.loginFragment)
                lifecycleScope.launchWhenResumed {
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToVerifyPhoneNumberFragment())
                }
        }
        binding.forgotPassword.setOnClickListener {
            lifecycleScope.launchWhenResumed { navController.navigate(R.id.passwordRestoreFragment) }
        }
        binding.buttonLogin.setOnClickListener {
            if (binding.telEditText.text?.isNotEmpty() == true && binding.passwordEditText.text?.isNotEmpty() == true) {
                viewModel.login(
                    sharedPrefsUtils = sharedPrefsUtils,
                    tel = binding.telEditText.text.toString(),
                    password = binding.passwordEditText.text.toString()
                )
            }
        }

        viewModel.accessAndRefreshToken.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarLogin.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.progressBarLogin.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                is OutcomeUi.Success -> {
                    if (it.data.keys.first().isNotEmpty() && it.data.values.first().isNotEmpty()) {
                        CoroutineScope(Dispatchers.Main).launch {
                            if (binding.checkboxLogin.isChecked) sharedPrefsUtils.setRemember(true)
                            println(
//                                    "34ss  access token ====  ${it.keys.first()}, refresh token === ${it.values.first()},  navController.currentDestination ==== ${navController.currentDestination?.id}, " +
//                                            "R.id.action_loginFragment_to_myOrdersFragment ==== ${R.id.action_loginFragment_to_myOrdersFragment}, checkboxLogin.isChecked = ${binding.checkboxLogin.isChecked}"
                            )
                            if (navController.currentDestination?.id == R.id.loginFragment) {
                                lifecycleScope.launchWhenResumed { navController.navigate(R.id.action_loginFragment_to_myOrdersFragment) }
                            } else {
                                lifecycleScope.launchWhenResumed { navController.navigate(R.id.myOrdersFragment) }
                            }
                        }
                    } else {
                        binding.progressBarLogin.visibility = View.GONE
                    }
                }

                else -> {}
            }
        }

        binding.superRootLogin.setOnClickListener { closeKeyBoard(it) }

        textFieldsBehavior()
    }


    private fun textFieldsBehavior() {
        binding.telEditText.addTextChangedListener {
            if (binding.telEditText.text?.isNotEmpty() == true && binding.passwordEditText.text?.isNotEmpty() == true)
                binding.buttonLogin.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            else binding.buttonLogin.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
            if (binding.telEditText.text.toString().length > 7)
                binding.telEditText.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
            else binding.telEditText.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        }
        binding.passwordEditText.addTextChangedListener {
            if (binding.telEditText.text?.isNotEmpty() == true && binding.passwordEditText.text?.isNotEmpty() == true)
                binding.buttonLogin.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            else binding.buttonLogin.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
            if (binding.passwordEditText.text.toString().length > 7)
                binding.passwordEditText.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
            else binding.passwordEditText.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        }
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}