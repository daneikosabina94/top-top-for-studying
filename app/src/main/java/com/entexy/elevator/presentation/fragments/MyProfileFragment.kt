package com.entexy.elevator.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.addCallback
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentMyProfileBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.ProfileViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MyProfileFragment : Fragment() {

    private lateinit var binding: FragmentMyProfileBinding
    private val viewModel by viewModel<ProfileViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMyProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.profileContainer.visibility = View.GONE

        val sharedPrefsUtils = SharedPrefsUtils(requireContext())
        viewModel.getProfile(sharedPrefsUtils)

        viewModel.mutableLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarProfile.visibility = View.VISIBLE
                is OutcomeUi.Failure -> if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                is OutcomeUi.Success -> {
                    binding.profileContainer.visibility = View.VISIBLE
                    binding.progressBarProfile.visibility = View.GONE
                    binding.profileUserName.text = it.data.firstName
                    binding.profileUserPhone.text = it.data.phoneNumber
                }

                else -> {}
            }
        }

        val sideFrameLayout = SideMenuFragment()
        childFragmentManager.beginTransaction().replace(R.id.motionLayoutContainer, sideFrameLayout).commit()
        installSideMenu(sideFrameLayout)

        binding.backArrow.setOnClickListener {
            findNavController().navigateUp()
        }

        requireActivity().onBackPressedDispatcher.addCallback {
            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
        }

        binding.exitProfile.setOnClickListener {
            viewModel.logout(sharedPrefsUtils)
        }
        viewModel.apiLogoutCode.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarProfile.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.progressBarProfile.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                else -> {
                    SharedPrefsUtils(requireContext()).setAccessToken("")
                    SharedPrefsUtils(requireContext()).setRemember(false)
                    SharedPrefsUtils(requireContext()).setUserPhone("")
                    SharedPrefsUtils(requireContext()).setUserName("")
                    findNavController().popBackStack(R.id.myOrdersFragment, true)
                    lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.loginFragment) }
                    findNavController().backQueue.clear()

                }
            }
        }

        binding.profileUserName.addTextChangedListener {
            sharedPrefsUtils.setUserName(binding.profileUserName.text.toString())
        }
        binding.profileUserPhone.addTextChangedListener {
            sharedPrefsUtils.setUserPhone(binding.profileUserPhone.text.toString())
        }
    }


    private fun installSideMenu(sideFrameLayout: SideMenuFragment) {
        binding.menuVertical.setOnClickListener {
            val motionLayoutContainer = binding.motionLayoutContainer
            with(sideFrameLayout.motionLayout) {
                val parentLayoutParams = motionLayoutContainer.layoutParams as FrameLayout.LayoutParams
                if (this?.progress == 0.0F) {
                    parentLayoutParams.marginStart = 0
                    motionLayoutContainer.layoutParams = parentLayoutParams
                    this.transitionToEnd()
                } else {
                    this?.transitionToStart()
                    parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                    motionLayoutContainer.layoutParams = parentLayoutParams
                }
                this?.addTransitionListener(object : MotionLayout.TransitionListener {
                    override fun onTransitionStarted(motionLayout: MotionLayout?, startId: Int, endId: Int) {}
                    override fun onTransitionChange(motionLayout: MotionLayout?, startId: Int, endId: Int, progress: Float) {}
                    override fun onTransitionTrigger(motionLayout: MotionLayout?, triggerId: Int, positive: Boolean, progress: Float) {}
                    override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                        if (this@with.progress == 0.0F) {
                            parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                            motionLayoutContainer.layoutParams = parentLayoutParams
                        }
                    }
                })
            }
        }
    }


}