package com.entexy.elevator.presentation

interface WeightClickListener {
    fun onWeightClick(s: String)
}

interface DimensClickListener {
    fun onDimensClick(s: String)
}

interface OrderClickListener {
    fun onOrderClick(s: Int)
}

interface ConfirmOrderListener {
    fun confirmOrder(orderId: Int)
}

interface CancelOrderListener {
    fun cancelOrder(orderId: Int)
}

interface CancelOrderWithCommission {
    fun cancelOrderWithCommission(orderId: Int)
}

interface UserDismissed {
    fun userDismissed()
}

interface RefreshOrdersListener {
    fun refreshOrders()
}