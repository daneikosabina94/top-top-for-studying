package com.entexy.elevator.presentation.activity.payment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import com.entexy.elevator.databinding.ActivityAlfaPaymentBinding

class AlfaPaymentActivity : AppCompatActivity() {
    private val binding by lazy { ActivityAlfaPaymentBinding.inflate(layoutInflater) }
    private val payUrl by lazy { intent.extras?.getString("url") ?: "" }

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (binding.webView.canGoBack()) binding.webView.goBack() else finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (payUrl.isNotEmpty()) initUi() else throw Exception("Empty url")
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initUi() = binding.webView.run {
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                when {
                    url.contains(SUCCESS_URL) -> setResult(AlfaPayState.Success)
                }
            }
        }
        loadUrl(payUrl)
    }

    override fun onResume() {
        super.onResume()
        binding.webView.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.webView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.webView.destroy()
        backPressedCallback.remove()
    }

    private fun setResult(state: AlfaPayState) {
        val data = Intent()
        data.putExtra("state", state.toString())
        setResult(RESULT_OK, data)
        finish()
    }

    companion object {
        fun start(activity: Activity, url: String): Intent {
            val intent = Intent(activity, Class.forName("com.entexy.elevator.presentation.activity.payment.AlfaPaymentActivity"))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("url", url)
            return intent
        }

        const val SUCCESS_URL = "https://www.success.pay.com"
    }
}