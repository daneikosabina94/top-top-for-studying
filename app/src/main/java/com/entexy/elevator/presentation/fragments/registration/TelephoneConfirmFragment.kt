package com.entexy.elevator.presentation.fragments.registration

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.receiver.DataBroadcastReceiverContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentTelConfirmBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.TelephoneConfirmViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class TelephoneConfirmFragment : Fragment(), DataBroadcastReceiverContract {

    private lateinit var binding: FragmentTelConfirmBinding
    private val viewModel by viewModel<TelephoneConfirmViewModel>()
    private val arguments by lazy {
        TelephoneConfirmFragmentArgs.fromBundle(requireArguments())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTelConfirmBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        TelContractProvider.setBroadcastContract(this)

        textFieldsBehavior()
        val sharedPrefsUtils = SharedPrefsUtils(requireContext())
        binding.nextTelConfirm.setOnClickListener {
            if (binding.telConfirmET.text?.isEmpty() == true) return@setOnClickListener
            viewModel.verifyOtp(
                sharedPrefsUtils = sharedPrefsUtils,
                otp = binding.telConfirmET.text.toString(),
                phone_number = sharedPrefsUtils.getUserPhone()
            )
        }
        viewModel.verifyOtpApiCode.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarTelConfirm.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.progressBarTelConfirm.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                else -> {
                    if (!arguments.isRestorePassword) {
                        if (findNavController().currentDestination?.id != null || findNavController().currentDestination?.label == "TelephoneConfirmFragment") {
                            lifecycleScope.launchWhenResumed {
                                findNavController().navigate(
                                    TelephoneConfirmFragmentDirections.actionTelephoneConfirmFragmentToRegistrationFragment(
                                        arguments.phoneNumber,
                                        binding.telConfirmET.text.toString()
                                    )
                                )
                            }
                        }
                    } else {
                        val bundle = Bundle()
                        bundle.putString("otp", binding.telConfirmET.text.toString())
                        if (findNavController().currentDestination?.id != null || findNavController().currentDestination?.label == "TelephoneConfirmFragment") {
                            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.passwordRestoreFragment, bundle) }
                        } else lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.passwordRestoreFragment, bundle) }
                    }
                    binding.progressBarTelConfirm.visibility = View.GONE
                }
            }
        }

        binding.superRootTelConfirm.setOnClickListener { closeKeyBoard(it) }

    }

    override fun getTel(s: String) {
        binding.telConfirmET.setText(s.subSequence(s.length - 4, s.length))
    }


    private fun textFieldsBehavior() {
        binding.telConfirmET.addTextChangedListener {
            if (binding.telConfirmET.text?.isNotEmpty() == true) {
                binding.nextTelConfirm.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
                binding.nextTelConfirm.isActivated = true
            } else {
                binding.nextTelConfirm.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
                binding.nextTelConfirm.isActivated = false
            }
        }
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


}