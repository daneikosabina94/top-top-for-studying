package com.entexy.elevator.presentation.helpers

import androidx.annotation.StringRes

sealed class OutcomeUi<out T> {
    class Success<out R>(val data: R) : OutcomeUi<R>()
    object Progress : OutcomeUi<Nothing>()
    object Empty : OutcomeUi<Nothing>()
    class Failure(@StringRes val idRes: Int) : OutcomeUi<Nothing>()
}
