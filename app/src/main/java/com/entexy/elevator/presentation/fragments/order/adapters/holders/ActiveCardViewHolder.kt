package com.entexy.elevator.presentation.fragments.order.adapters.holders

import androidx.recyclerview.widget.RecyclerView
import com.entexy.elevator.databinding.RecyclerItemCardActiveBinding

class ActiveCardViewHolder(val binding: RecyclerItemCardActiveBinding) : RecyclerView.ViewHolder(binding.root)