package com.entexy.elevator.presentation.helpers

import android.app.ActionBar.LayoutParams
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.entexy.elevator.R
import com.entexy.elevator.databinding.FragmentPicksViewerBinding
import com.entexy.elevator.databinding.ImageViewerItemBinding


class ImageViewerFragment :  DialogFragment() {

    private lateinit var binding: FragmentPicksViewerBinding

    override fun onResume() {
        super.onResume()

        closeKeyBoard(binding.root)

//        val displayMetrics = requireContext().resources.displayMetrics
//        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        binding.parentframe.layoutParams  = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,1000)
        binding.parentframe.background = ContextCompat.getDrawable(requireContext(), R.drawable.back_image_viewer)
        dialog?.window?.setLayout(LayoutParams.MATCH_PARENT, 1000)
        dialog?.window?.setGravity(Gravity.CENTER)
        dialog?.window?.setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.back_image_viewer))
//        dialog?.window?.setBackgroundDrawable(Color.BLACK)


        val viewPager2 = binding.viewPager
        viewPager2.foregroundGravity = Gravity.CENTER
        val pathsList = requireArguments().getStringArrayList("button_pressed_key")
        val position = requireArguments().getInt("position", 0)
        viewPager2.adapter = ViewPagerAdapter(requireContext(), pathsList!!)
        viewPager2.setCurrentItem(position, false)

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = FragmentPicksViewerBinding.inflate(LayoutInflater.from(requireContext()))
        return AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .create()
    }


    class ViewPagerAdapter(val context: Context, val array: ArrayList<String>?) : RecyclerView.Adapter<ViewPagerAdapter.PagerVH>() {
        private lateinit var binding: ImageViewerItemBinding
        override fun getItemCount(): Int = array!!.size
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
            binding = ImageViewerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return PagerVH(binding)
        }
        override fun onBindViewHolder(holder: PagerVH, position: Int) {
            val uri = array?.get(position)
            if (uri?.isEmpty() == true) return
            if (uri?.startsWith("htt") == true) {
                Glide.with(context)
                    .load(uri)
                    .apply(RequestOptions().centerCrop())
                    .apply(RequestOptions().override(1000, 1200))
                    .into(holder.photoView)
            } else if (uri?.startsWith("content") == true) {
//                holder.photoView.setImageURI(Uri.parse(array?.get(position)))
                Glide.with(context)
                    .load(Uri.parse(uri))
                    .apply(RequestOptions().centerCrop())
                    .apply(RequestOptions().override(1000, 1200))
                    .into(holder.photoView)
            } else {
                val b = Base64.decode(uri, Base64.DEFAULT)
                val bitmap = BitmapFactory.decodeByteArray(b, 0, b.size)
                holder.photoView.setImageBitmap(bitmap)
            }
        }
        inner class PagerVH(binding: ImageViewerItemBinding) : RecyclerView.ViewHolder(binding.root) {
            var photoView = binding.photoView
        }
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}




