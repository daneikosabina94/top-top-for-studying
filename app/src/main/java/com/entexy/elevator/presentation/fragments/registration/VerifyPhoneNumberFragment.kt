package com.entexy.elevator.presentation.fragments.registration

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentVerifyPhoneNumberBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.RegistrationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class VerifyPhoneNumberFragment : Fragment() {
    private val binding by lazy {
        FragmentVerifyPhoneNumberBinding.inflate(layoutInflater)
    }
    private val viewModel by viewModel<RegistrationViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pref = SharedPrefsUtils(requireContext())
        binding.entrance.setOnClickListener {
            findNavController().popBackStack()
        }
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack()
            }
        })
        binding.phoneNumber.doAfterTextChanged {
            changeButtonBackground(it.toString().isNotBlank())
        }
        binding.buttonRegistration.setOnClickListener {
            if (binding.phoneNumber.text?.isEmpty() == true) return@setOnClickListener
            viewModel.verifyPhone(
                sharedPrefsUtils = pref,
                phone_number = binding.phoneNumber.text.toString(),
                from = "registration"
            )
        }
        binding.superRootRegistration.setOnClickListener { closeKeyBoard(it) }
        viewModel.verifyPhoneApiCode.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarRegistration.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.progressBarRegistration.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                else -> {
                    pref.setUserPhone(binding.phoneNumber.text.toString())
                    lifecycleScope.launchWhenResumed {
                        findNavController().navigate(
                            VerifyPhoneNumberFragmentDirections.actionVerifyPhoneNumberFragmentToTelephoneConfirmFragment(
                                binding.phoneNumber.text.toString(),
                                false
                            )
                        )
                    }

                    binding.progressBarRegistration.visibility = View.GONE
                }
            }
        }
    }


    private fun changeButtonBackground(enabled: Boolean) = with(binding) {
        if (enabled) {
            buttonRegistration.apply {
                background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
                isEnabled = true
                isActivated = true
            }
        } else {
            buttonRegistration.apply {
                background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
                isEnabled = false
                isActivated = false
            }
        }
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}