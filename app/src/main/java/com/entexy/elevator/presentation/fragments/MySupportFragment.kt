package com.entexy.elevator.presentation.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.addCallback
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.databinding.FragmentMySupportBinding


class MySupportFragment : Fragment() {

    private lateinit var binding: FragmentMySupportBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMySupportBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sideFrameLayout = SideMenuFragment()
        childFragmentManager.beginTransaction().replace(R.id.motionLayoutContainer, sideFrameLayout).commit()
        installSideMenu(sideFrameLayout)


        binding.backArrow.setOnClickListener {
            lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.myOrdersFragment)}
        }

        requireActivity().onBackPressedDispatcher.addCallback {
            lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.myOrdersFragment)}
        }


        binding.supportTel.setOnClickListener {
            val mobileNumber = "+7 000 00 00"
            val intent = Intent()
            intent.action = Intent.ACTION_DIAL // Action for what intent called for
            intent.data = Uri.parse("tel: $mobileNumber") // Data with intent respective action on intent
            startActivity(intent)
        }

        binding.supportMail.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("mailto:Podgruz@mail.ru")
            startActivity(Intent.createChooser(intent, "E-mail"))
        }




    }



    private fun installSideMenu(sideFrameLayout: SideMenuFragment) {
        binding.menuVertical.setOnClickListener {
            val motionLayoutContainer = binding.motionLayoutContainer
            with(sideFrameLayout.motionLayout) {
                val parentLayoutParams = motionLayoutContainer.layoutParams as FrameLayout.LayoutParams
                if (this?.progress == 0.0F) {
                    parentLayoutParams.marginStart = 0
                    motionLayoutContainer.layoutParams = parentLayoutParams
                    this.transitionToEnd()
                } else {
                    this?.transitionToStart()
                    parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                    motionLayoutContainer.layoutParams = parentLayoutParams
                }
                this?.addTransitionListener(object : MotionLayout.TransitionListener {
                    override fun onTransitionStarted(motionLayout: MotionLayout?, startId: Int, endId: Int) {}
                    override fun onTransitionChange(motionLayout: MotionLayout?, startId: Int, endId: Int, progress: Float) {}
                    override fun onTransitionTrigger(motionLayout: MotionLayout?, triggerId: Int, positive: Boolean, progress: Float) {}
                    override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                        if (this@with.progress == 0.0F) {
                            parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                            motionLayoutContainer.layoutParams = parentLayoutParams
                        }
                    }
                })
            }
        }
    }




}