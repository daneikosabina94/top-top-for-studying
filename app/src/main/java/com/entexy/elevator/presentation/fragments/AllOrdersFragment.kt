package com.entexy.elevator.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.addCallback
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.entexy.elevator.App
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.EntityOrderBinding
import com.entexy.elevator.databinding.FragmentMyOrdersBinding
import com.entexy.elevator.domain.models.OneOrderShortInfoDomain
import com.entexy.elevator.presentation.OrderClickListener
import com.entexy.elevator.presentation.RefreshOrdersListener
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.GetOrdersViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


class AllOrdersFragment : Fragment(), OrderClickListener, RefreshOrdersListener {

    private lateinit var binding: FragmentMyOrdersBinding
    private val viewModel by viewModel<GetOrdersViewModel>()
    private lateinit var sharedPrefsUtils: SharedPrefsUtils
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: OrdersAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMyOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.refreshOrdersListener = this

        val currentBavBackStackEntry = findNavController().currentBackStackEntry

        if (findNavController().previousBackStackEntry?.destination?.label == "LoginFragment" ||
            findNavController().previousBackStackEntry?.destination?.label == "RegistrationFragment"
        ) {
            findNavController().backQueue.clear()
            if (currentBavBackStackEntry != null) findNavController().backQueue.add(currentBavBackStackEntry)
        }
        lifecycleScope.launchWhenResumed { requireActivity().onBackPressedDispatcher.addCallback { lifecycleScope.launchWhenResumed { requireActivity().finish() } } }

        sharedPrefsUtils = SharedPrefsUtils(requireContext())

        installSideMenu()

        recyclerView = binding.oredersRecycler
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        getOrders()
        viewModel.getAllBindings(sharedPrefsUtils)
        viewModel.getAllBindingsStatus.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Success -> {
                    if (sharedPrefsUtils.getCurrentBindingId().isNullOrEmpty()) {
                        Log.d("CardsBinding", "Pref Null")
                        if (it.data.isNotEmpty()) {
                            sharedPrefsUtils.setCurrentBindingId(it.data[0].bindingId)
                            it.data[0].isCurrent
                            Log.d("CardsBinding", "Data is not empty ${it.data[0]}")
                        }
                    } else {
                        Log.d("CardsBinding", "Pref not null")
                        val currentId = it.data.find { card ->
                            card.bindingId == sharedPrefsUtils.getCurrentBindingId()
                        }
                        Log.d("CardsBinding", "Current card $currentId")
                        if (currentId == null) {
                            it.data.forEach {
                                it.isCurrent = false
                                Log.d("CardsBinding", "Each item $it")
                            }
                            sharedPrefsUtils.setCurrentBindingId(it.data[0].bindingId)
                            it.data[0].isCurrent = true
                            Log.d("CardsBinding", "new Set current ${it.data[0]}")
                        }
                    }
                }

                else -> {
                }
            }
        }

        binding.butnPlus.setOnClickListener {
            binding.progressBarMyOrders.visibility = View.VISIBLE
            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.orderRegistrationFragment) }
        }

        binding.swiperefresh.setOnRefreshListener {
            getOrders()
        }
        viewModel.orders.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarMyOrders.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.swiperefresh.isRefreshing = false
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }
                is OutcomeUi.Success -> {
                    if (it.data.isNotEmpty()) {
                        binding.oredersRecycler.visibility = View.VISIBLE
                        binding.regOrderCardView.visibility = View.GONE
                        binding.progressBarMyOrders.visibility = View.GONE
                    } else {
                        binding.regOrderCardView.visibility = View.VISIBLE
                        binding.progressBarMyOrders.visibility = View.GONE
                    }
                    val sortedListOrders = it.data.sortedByDescending { it.status }.toMutableList()
                    adapter = OrdersAdapter(requireContext(), sortedListOrders, this)
                    recyclerView.adapter = adapter
                    App.startAnim(binding.butnPlus)
                    binding.swiperefresh.isRefreshing = false
                }

                else -> {}
            }
        }
    }

    private fun getOrders() {
        viewModel.getOrders(sharedPrefsUtils)
    }


    class OrdersAdapter(
        private val context: Context,
        private val mutableList: MutableList<OneOrderShortInfoDomain>,
        private val clickListener: OrderClickListener
    ) :
        RecyclerView.Adapter<OrdersAdapter.PagerVH>() {
        private lateinit var binding: EntityOrderBinding
        override fun getItemCount() = mutableList.size
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
            binding = EntityOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return PagerVH(binding)
        }

        override fun onBindViewHolder(holder: PagerVH, position: Int) {
            val oneOrderShortInfo = mutableList[position]
            holder.orderName.text = oneOrderShortInfo.title
            holder.addressValue.text = oneOrderShortInfo.address
            holder.humans_count.text = oneOrderShortInfo.predictedMoversCount.toString()
            holder.cargoType.text = when (oneOrderShortInfo.dimensions) {
                "small_sized" -> "Малогабаритный"
                "medium_sized" -> "Среднегабаритный"
                "large_sized" -> "Крупногабаритный"
                else -> "Не определено"
            }
            holder.weightValue.text = when (oneOrderShortInfo.weight) {
                "light" -> "Легко"
                "medium" -> "Средне"
                "heavy" -> "Тяжело"
                "very_heavy" -> "Очень тяжело"
                else -> "Не определено"
            }
            holder.predictPrice.text = holder.predictPrice.context.getString(R.string.order_price, oneOrderShortInfo.price.toString())
            val input = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val date = oneOrderShortInfo.created_at?.let { input.parse(it) }
            val cal = Calendar.getInstance()
            if (date != null) {
                cal.time = date
            }
            holder.orderEntityDateTv.text = "${cal.get(Calendar.DAY_OF_MONTH)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.YEAR)}    "

            when (oneOrderShortInfo.status) {
                "on_the_way" -> {
                    holder.inWayView.visibility = View.VISIBLE
                    holder.inWayView.setImageResource(R.drawable.status_inway)
                }

                "performed" -> {
                    holder.inWayView.visibility = View.VISIBLE
                    holder.inWayView.setImageResource(R.drawable.status_waiting)
                }

                "confirmed" -> {
                    holder.inWayView.visibility = View.VISIBLE
                    holder.inWayView.setImageResource(R.drawable.status_confirmed)
                }

                "cancelled" -> {
                    holder.inWayView.visibility = View.VISIBLE
                    holder.inWayView.setImageResource(R.drawable.status_canceled)
                }

                else -> {
                    holder.inWayView.visibility = View.VISIBLE
                    holder.inWayView.setImageResource(R.drawable.status_order_created)
                }
            }
            holder.cardContainerOrder.setOnClickListener {
                holder.progressBarOneOrderEntity.visibility = View.VISIBLE
                clickListener.onOrderClick(oneOrderShortInfo.id)
            }
        }

        inner class PagerVH(binding: EntityOrderBinding) : RecyclerView.ViewHolder(binding.root) {
            var orderName = binding.orderName
            var addressValue = binding.addressValue
            var humans_count = binding.humansCount
            var cargoType = binding.cargoType
            var weightValue = binding.weightValue
            var predictPrice = binding.predictPrice
            var cardContainerOrder = binding.cardContainerOrder
            var progressBarOneOrderEntity = binding.progressBarOneOrderEntity
            var inWayView = binding.inWayView
            var orderEntityDateTv = binding.orderEntityDateTv
        }
    }

    override fun onOrderClick(orderId: Int) {
        val bundle = Bundle()
        bundle.putInt("orderId", orderId)
        lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.orderRegistrationFragment, bundle) }
    }


    private fun installSideMenu() {
        val sideFrameLayout = SideMenuFragment()
        childFragmentManager.beginTransaction().replace(R.id.motionLayoutContainer, sideFrameLayout).commit()
        binding.menuVertical.setOnClickListener {
            val motionLayoutContainer = binding.motionLayoutContainer
            with(sideFrameLayout.motionLayout) {
                val parentLayoutParams = motionLayoutContainer.layoutParams as FrameLayout.LayoutParams
                if (this?.progress == 0.0F) {
                    parentLayoutParams.marginStart = 0
                    motionLayoutContainer.layoutParams = parentLayoutParams
                    this.transitionToEnd()
                } else {
                    this?.transitionToStart()
                    parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                    motionLayoutContainer.layoutParams = parentLayoutParams
                }
                this?.addTransitionListener(object : MotionLayout.TransitionListener {
                    override fun onTransitionStarted(motionLayout: MotionLayout?, startId: Int, endId: Int) {}
                    override fun onTransitionChange(motionLayout: MotionLayout?, startId: Int, endId: Int, progress: Float) {}
                    override fun onTransitionTrigger(motionLayout: MotionLayout?, triggerId: Int, positive: Boolean, progress: Float) {}
                    override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                        if (this@with.progress == 0.0F) {
                            parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                            motionLayoutContainer.layoutParams = parentLayoutParams
                        }
                    }
                })
            }
        }
    }

    override fun refreshOrders() {
        lifecycleScope.launchWhenResumed {
            lifecycleScope.launch(Dispatchers.Main) {
                getOrders()
            }
        }
    }


}