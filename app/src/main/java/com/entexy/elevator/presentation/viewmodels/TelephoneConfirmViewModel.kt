package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.TelephoneConfirmUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TelephoneConfirmViewModel(val telephoneConfirmUseCase: TelephoneConfirmUseCase) : ViewModel() {

    val verifyOtpApiCode = MutableLiveData<OutcomeUi<Unit>>()

    fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String) = viewModelScope.launch(Dispatchers.IO) {
        telephoneConfirmUseCase.verifyOtp(sharedPrefsUtils = sharedPrefsUtils, otp = otp, phone_number = phone_number).collect {
            verifyOtpApiCode.postValue(it)
        }
    }
}