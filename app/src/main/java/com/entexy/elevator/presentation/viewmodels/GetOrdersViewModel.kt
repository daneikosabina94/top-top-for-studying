package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.models.OneOrderShortInfoDomain
import com.entexy.elevator.domain.uscases.GetAllBindingsUseCase
import com.entexy.elevator.domain.uscases.GetOrdersUseCase
import com.entexy.elevator.presentation.fragments.order.adapters.item.CardItem
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GetOrdersViewModel(
    val myOrdersUseCase: GetOrdersUseCase,
    private val getAllBindingsUseCase: GetAllBindingsUseCase,
) : ViewModel() {

    val orders = MutableLiveData<OutcomeUi<MutableList<OneOrderShortInfoDomain>>>()
    private val _getAllBindingsStatus = MutableLiveData<OutcomeUi<List<CardItem>>>()
    val getAllBindingsStatus: LiveData<OutcomeUi<List<CardItem>>> = _getAllBindingsStatus
    fun getOrders(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        myOrdersUseCase.getOrders(sharedPrefsUtils = sharedPrefsUtils).collect {
            orders.postValue(it)
        }
    }

    fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        getAllBindingsUseCase.getAllBindings(sharedPrefsUtils).collect {
            _getAllBindingsStatus.postValue(it)
        }
    }


}