package com.entexy.elevator.presentation.fragments.order

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.entexy.elevator.App
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentOrderRegistrationLayoutBinding
import com.entexy.elevator.databinding.ViewpagerDimensionsItemBinding
import com.entexy.elevator.databinding.ViewpagerWeightItemBinding
import com.entexy.elevator.presentation.*
import com.entexy.elevator.presentation.activity.payment.AlfaPayState
import com.entexy.elevator.presentation.activity.payment.AlfaPaymentActivity
import com.entexy.elevator.presentation.fragments.SideMenuFragment
import com.entexy.elevator.presentation.fragments.order.adapters.AddressesAdapter
import com.entexy.elevator.presentation.fragments.order.adapters.CardsAdapter
import com.entexy.elevator.presentation.fragments.order.adapters.callbacks.OnAddressClickCallback
import com.entexy.elevator.presentation.helpers.CustomDialogFragment
import com.entexy.elevator.presentation.helpers.ImageViewerFragment
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.OrderRegistrationViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class OrderRegistrationFragment : Fragment(), DimensClickListener, WeightClickListener, ConfirmOrderListener, CancelOrderListener,
    CancelOrderWithCommission, UserDismissed {

    private val sharedPrefsUtils by lazy { SharedPrefsUtils(requireContext()) }
    private var imageUri: Uri? = null
    private val IMAGE_PICK = 11
    private var photoCellNumber = 0
    private lateinit var binding: FragmentOrderRegistrationLayoutBinding
    private val viewModel by viewModel<OrderRegistrationViewModel>()

    private var photosByteArrayList = mutableListOf<ByteArray>()
    private var photosUrlPaths = arrayListOf("", "", "")
    private var viewPagerDimensios = -1
    private var pagerWeight = -1
    private var geoResults: MutableList<Address>? = null
    private var onDimensClickPress = ""
    private var onWeightClickPress = ""
    private var exitAttempts = 0
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private val cardsAdapter by lazy { CardsAdapter(sharedPrefsUtils) }
    private val addressAdapter by lazy {
        AddressesAdapter(object : OnAddressClickCallback {
            override fun onAddressClick(address: String) {
                binding.orderRegistrationLayout.address.removeTextChangedListener(textWatcher)
                binding.orderRegistrationLayout.address.setText(
                    address
                )
                binding.orderRegistrationLayout.spinnerTV.visibility = View.GONE
                binding.orderRegistrationLayout.address.addTextChangedListener(textWatcher)
            }
        })
    }
    private lateinit var textWatcher: TextWatcher


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentOrderRegistrationLayoutBinding.inflate(inflater, container, false)
        initBottomSheetBehavior()
        return binding.root
    }

    private fun initBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.cardsBottomSheet.root)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
        binding.cardsBottomSheet.rvCards.adapter = cardsAdapter
        binding.orderRegistrationLayout.spinnerTV.adapter = addressAdapter
        viewModel.getAllBindings(sharedPrefsUtils)
    }

    private fun initCardsBottomSheet() = with(binding.cardsBottomSheet) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        initCardsBottomSheetUi()
    }

    private fun initCardsBottomSheetUi() = with(binding.cardsBottomSheet) {
        llAddCard.setOnClickListener {
            viewModel.createBinding(sharedPrefsUtils)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        btnNext.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private val contract =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.extras?.getString("state").let { state ->
                    val type = AlfaPayState.valueOf(state!!)
                    if (type == AlfaPayState.Success)
                        viewModel.verifyBinding(sharedPrefsUtils)
                }
            }
        }

    @SuppressLint("MissingPermission", "ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        installSideMenu()
        binding.orderRegistrationLayout.buttonPayment.setOnClickListener {
            initCardsBottomSheet()
        }
        viewModel.verifyBindingStatus.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> {
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.VISIBLE
                }

                is OutcomeUi.Failure -> {
                    Toast.makeText(
                        requireContext(),
                        if (it.idRes != -1) getString(it.idRes) else getString(R.string.unknown_error),
                        Toast.LENGTH_LONG
                    ).show()
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                }

                is OutcomeUi.Success -> {
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                    viewModel.getAllBindings(sharedPrefsUtils)
                }

                else -> {}
            }
        }
        viewModel.createBindingStatus.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Success -> {
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                    contract.launch(
                        AlfaPaymentActivity.start(
                            requireActivity(),
                            it.data.formUrl
                        )
                    )
                }

                is OutcomeUi.Failure -> {
                    Toast.makeText(
                        requireContext(),
                        if (it.idRes != -1) getString(it.idRes) else getString(R.string.unknown_error),
                        Toast.LENGTH_LONG
                    ).show()
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                }

                is OutcomeUi.Progress -> binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.VISIBLE
                else -> binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
            }
        }

        viewModel.getAllBindingsStatus.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Success -> {
                    cardsAdapter.submitList(it.data)
                }

                is OutcomeUi.Failure -> {
                }

                else -> {}
            }
        }
        binding.orderRegistrationLayout.backArrow.setOnClickListener { lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) } }
        requireActivity().onBackPressedDispatcher.addCallback {
            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
        }

        installDimensionsAndWeightViewPagers()

        installPhotoContainers()

        if (arguments == null) {

            installAddressGeocoderAndListenrs()

            binding.orderRegistrationLayout.photoBrigadierName.visibility = View.GONE
            binding.orderRegistrationLayout.brigadierPhotoNameContainer.visibility = View.GONE
            binding.orderRegistrationLayout.orderInWork.visibility = View.GONE
            binding.orderRegistrationLayout.paymentDoneContainer.visibility = View.GONE

            binding.orderRegistrationLayout.photoFromDisk.setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(Intent.createChooser(intent, "Select images"), IMAGE_PICK)
            }
            binding.orderRegistrationLayout.photoFromCamera.setOnClickListener {
                try {
                    val values = ContentValues()
                    values.put(MediaStore.MediaColumns.DISPLAY_NAME, "image.jpg")
                    values.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                    values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                    imageUri = requireContext().contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                    startActivityForResult(takePictureIntent, IMAGE_PICK)
                } catch (ex: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        "Не найдено приложения камеры по умолчанию. Попробуйте через галерею",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            binding.orderRegistrationLayout.buttonPlaceOrder.setOnClickListener {
                var city = "Район не определен"
                var district = "Район не определен"
                var address_latitude = 0.0
                var address_longitude = 0.0
                var houseNumber = ""
                try {
                    Log.d("GeoGeo", "$geoResults")
                    if (geoResults != null && geoResults?.isNotEmpty() == true) {
                        city = geoResults!![0].locality
                        district = geoResults!![0].subLocality ?: " "
                        address_latitude = geoResults!![0].latitude
                        address_longitude = geoResults!![0].longitude
                        houseNumber = geoResults!![0].subThoroughfare
                    }
                } catch (exception: Exception) {
                    Log.d("GeoGeo", exception.stackTraceToString())
                    makeToast<String>("Не указан номер дома или иная информация", "")
                    return@setOnClickListener
                }

                if ((onDimensClickPress == "press") && (onWeightClickPress == "press")) {
                    when {
                        houseNumber.isEmpty() -> {
                            makeToast<String>("Не указан номер дома", "").apply { return@setOnClickListener }
                        }

                        sharedPrefsUtils.getCurrentBindingId()!!.isEmpty() -> {
                            makeToast<String>("Не выбран способ оплаты", "").apply { return@setOnClickListener }
                        }
                    }
                    viewModel.postOrder(
                        sharedPrefsUtils = sharedPrefsUtils,
                        photosByteArrayList = photosByteArrayList,
                        city = city,
                        address = if (binding.orderRegistrationLayout.address.text?.isNotEmpty() == true) "кв. " + binding.orderRegistrationLayout.flat.text.toString() + ", " + binding.orderRegistrationLayout.address.text.toString()
                        else makeToast<String>("Введите адрес", "").apply { return@setOnClickListener },
                        district = district,
                        address_latitude = address_latitude,
                        address_longitude = address_longitude,
                        entrance = if (binding.orderRegistrationLayout.entrance2.text?.isNotEmpty() == true) binding.orderRegistrationLayout.entrance2.text.toString()
                            .toInt()
                        else makeToast<Int>(
                            binding.orderRegistrationLayout.entrance2.hint.toString(), 0
                        ).apply { return@setOnClickListener },
                        floor = if (binding.orderRegistrationLayout.floor.text?.isNotEmpty() == true) binding.orderRegistrationLayout.floor.text.toString()
                            .toInt()
                        else makeToast<Int>(
                            binding.orderRegistrationLayout.floor.hint.toString(), 0
                        ).apply { return@setOnClickListener },
                        is_there_an_elevator = binding.orderRegistrationLayout.isThereAnElevator.isChecked,
                        title = if (binding.orderRegistrationLayout.title.text?.isNotEmpty() == true) binding.orderRegistrationLayout.title.text.toString()
                        else makeToast<String>(
                            binding.orderRegistrationLayout.title.hint.toString(), ""
                        ).apply { return@setOnClickListener },
                        items_count = if (binding.orderRegistrationLayout.itemsCount.text?.isNotEmpty() == true) binding.orderRegistrationLayout.itemsCount.text.toString()
                            .toInt()
                        else makeToast<Int>(
                            binding.orderRegistrationLayout.itemsCount.hint.toString(), 0
                        ).apply { return@setOnClickListener },
                        lifts_count = if (binding.orderRegistrationLayout.liftsCount.text?.isNotEmpty() == true) binding.orderRegistrationLayout.liftsCount.text.toString()
                            .toInt()
                        else makeToast<Int>(
                            binding.orderRegistrationLayout.liftsCount.hint.toString(), 0
                        ).apply { return@setOnClickListener },
                        comment = if (binding.orderRegistrationLayout.comment.text?.isNotEmpty() == true) binding.orderRegistrationLayout.comment.text.toString() else "",
                        bring_to_floor = false,
                        dimensions = when (viewPagerDimensios) {
                            0 -> "small_sized"
                            1 -> "medium_sized"
                            2 -> "large_sized"
                            else -> ""
                            /** тут потом с текстовой вьюхи берем, которая включается при ручном вводе */
                        },
                        weight = when (pagerWeight) {
                            0 -> "light"
                            1 -> "medium"
                            2 -> "heavy"
                            3 -> "very_heavy"
                            else -> ""
                            /** тут потом с текстовой вьюхи берем, которая включается при ручном вводе */
                        },
                        price = 2,
                        predicted_movers_count = 1,
                        payment_card = sharedPrefsUtils.getCurrentBindingId()!!
                    )
                } else {
                    makeToast("Выберите размеры/вес", "")
                }
            }
            installBottomSheetsDrugAnimation()
            checkExitBehaviour()
            binding.orderRegistrationLayout.buttonFinish.visibility = View.GONE
            binding.orderRegistrationLayout.cancelOrder.visibility = View.INVISIBLE
        }

        if (arguments != null && !requireArguments().isEmpty) {
            binding.orderRegistrationLayout.mainTitle.text = "Просмотр заявки"
            binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.VISIBLE
            binding.orderRegistrationLayout.superParentOrderRegistration.visibility = View.INVISIBLE
            binding.orderRegistrationLayout.flatContainer.visibility = View.GONE
            binding.orderRegistrationLayout.flatContainerSeparator.visibility = View.GONE

            viewModel.getOrder(sharedPrefsUtils, requireArguments().getInt("orderId", 0))
        }

        binding.orderRegistrationLayout.dimensionsArmInput.setOnCheckedChangeListener { compoundButton, checked ->
            closeKeyBoard(compoundButton)
            if (checked) binding.orderRegistrationLayout.bottomSheetDimension.visibility = View.VISIBLE
            else binding.orderRegistrationLayout.bottomSheetDimension.visibility = View.GONE
        }
        binding.orderRegistrationLayout.weightArmInput.setOnCheckedChangeListener { compoundButton, checked ->
            closeKeyBoard(compoundButton)
            if (checked) binding.orderRegistrationLayout.bottomSheetWeight.visibility = View.VISIBLE
            else binding.orderRegistrationLayout.bottomSheetWeight.visibility = View.GONE
        }
        binding.orderRegistrationLayout.buttonSaveDimensions.setOnClickListener {
            closeKeyBoard(it)
            binding.orderRegistrationLayout.bottomSheetDimension.visibility = View.GONE
            binding.orderRegistrationLayout.viewPagerDimensions.setCurrentItem(2, true)
            (binding.orderRegistrationLayout.viewPagerDimensions.adapter as ViewPagerAdapterDimensions).pressCurrent(2)
        }
        binding.orderRegistrationLayout.buttonSaveWeight.setOnClickListener {
            closeKeyBoard(it)
            binding.orderRegistrationLayout.bottomSheetWeight.visibility = View.GONE
            binding.orderRegistrationLayout.viewPagerWeight.setCurrentItem(2, true)
            (binding.orderRegistrationLayout.viewPagerWeight.adapter as ViewPagerAdapterWeight).pressCurrent(2)
        }

        installTextFieldsBehavior()

        binding.orderRegistrationLayout.comment.imeOptions = EditorInfo.IME_ACTION_NEXT;
        binding.orderRegistrationLayout.comment.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        binding.orderRegistrationLayout.address.imeOptions = EditorInfo.IME_ACTION_NEXT;
        binding.orderRegistrationLayout.address.nextFocusDownId = R.id.flat
        binding.orderRegistrationLayout.address.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        initListeners()

        binding.orderRegistrationLayout.superParentOrderRegistration.setOnClickListener { closeKeyBoard(it) }

    }

    private fun initListeners() = with(binding.orderRegistrationLayout) {
        viewModel.apiAnswer.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> progressBarOrderRegistration.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    progressBarOrderRegistration.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                else -> lifecycleScope.launchWhenResumed { findNavController().navigateUp() }
            }
        }

        viewModel.orders.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> progressBarOrderRegistration.visibility = View.VISIBLE
                is OutcomeUi.Failure -> if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                is OutcomeUi.Success -> {
                    binding.orderRegistrationLayout.superParentOrderRegistration.visibility = View.VISIBLE

                    binding.orderRegistrationLayout.address.setText(it.data.address)
                    disableEditText(
                        binding.orderRegistrationLayout.address
                    )
                    binding.orderRegistrationLayout.entrance2.setText(it.data.entrance.toString())
                    disableEditText(
                        binding.orderRegistrationLayout.entrance2
                    )
                    binding.orderRegistrationLayout.floor.setText(it.data.floor.toString())
                    disableEditText(
                        binding.orderRegistrationLayout.floor
                    )
                    if (it.data.isThereAnElevator == true) {
                        binding.orderRegistrationLayout.isThereAnElevator.setButtonDrawable(R.drawable.ic_checkbox_gray)
                    } else binding.orderRegistrationLayout.isThereAnElevator.setButtonDrawable(R.drawable.ic_checkbox_gray_unchecked)
                    binding.orderRegistrationLayout.dimensionsArmInputContainer.visibility = View.GONE
                    binding.orderRegistrationLayout.weightArmInputContainer.visibility = View.GONE
                    binding.orderRegistrationLayout.title.setText(it.data.title)
                    disableEditText(
                        binding.orderRegistrationLayout.title
                    )
                    binding.orderRegistrationLayout.itemsCount.setText(it.data.itemsCount.toString())
                    disableEditText(
                        binding.orderRegistrationLayout.itemsCount
                    )
                    binding.orderRegistrationLayout.liftsCount.setText(it.data.liftsCount.toString())
                    disableEditText(
                        binding.orderRegistrationLayout.liftsCount
                    )
                    binding.orderRegistrationLayout.comment.setText(it.data.comment)
                    disableEditText(
                        binding.orderRegistrationLayout.comment
                    )
                    binding.orderRegistrationLayout.viewPagerDimensions.currentItem = when (it.data.dimensions) {
                        "small_sized" -> 0
                        "medium_sized" -> 1
                        "large_sized" -> 2
                        else -> 0
                    }
                    binding.orderRegistrationLayout.viewPagerWeight.currentItem = when (it.data.weight) {
                        "light" -> 0
                        "medium" -> 1
                        "heavy" -> 2
                        "very_heavy" -> 3
                        else -> 0
                    }
                    binding.orderRegistrationLayout.photoCategoryName.text = "Фото груза"
                    val circularProgressDrawable = App.circularProgressDrawable
                    circularProgressDrawable.start()
                    it.data.photos.forEachIndexed { index, path ->
                        println("36ss index = " + index)
                        println("36ss path = " + path)

                        if (index < 3) photosUrlPaths[index] = path.imageUrl

                        Glide.with(requireContext())
                            .load(path.imageUrl)
                            .placeholder(circularProgressDrawable)
                            .apply(RequestOptions().centerCrop())
                            .apply(RequestOptions().override(400, 400))
                            .into(
                                when (index) {
                                    0 -> binding.orderRegistrationLayout.addPhotoContainerOne.also {
                                        binding.orderRegistrationLayout.addPhotoContainerOne.tag = path.imageUrl
                                    }

                                    1 -> binding.orderRegistrationLayout.addPhotoContainerTwo.also {
                                        binding.orderRegistrationLayout.addPhotoContainerTwo.tag = path.imageUrl
                                    }

                                    2 -> binding.orderRegistrationLayout.addPhotoContainertThree.also {
                                        binding.orderRegistrationLayout.addPhotoContainertThree.tag = path.imageUrl
                                    }

                                    else -> binding.orderRegistrationLayout.addPhotoContainertThree.also {
                                        binding.orderRegistrationLayout.addPhotoContainertThree.tag = path.imageUrl
                                    }
                                }
                            )
                    }
                    Glide.with(requireContext())
                        .load(if (it.data.brigadier?.photo != null) it.data.brigadier?.photo?.imageUrl else R.drawable.ic_placeholder)
                        .placeholder(circularProgressDrawable)
                        .apply(RequestOptions().centerCrop())
                        .apply(RequestOptions().override(400, 400))
                        .into(
                            binding.orderRegistrationLayout.brigadierPhoto
                        )

                    if (it.data.brigadier?.firstName == null) {
                        binding.orderRegistrationLayout.photoBrigadierName.visibility = View.GONE
                        binding.orderRegistrationLayout.brigadierPhotoNameContainer.visibility = View.GONE
                    }

                    binding.orderRegistrationLayout.brigadierName.text =
                        it.data.brigadier?.lastName + " " + it.data.brigadier?.firstName + " " + it.data.brigadier?.middleName
                    binding.orderRegistrationLayout.buttonPayment.visibility = View.GONE
                    binding.orderRegistrationLayout.paymentDetailed.text = it.data.paymentCard
                    binding.orderRegistrationLayout.totalValueDetailed.text = getString(R.string.order_price, it.data.price ?: "")
                    val movers =
                        if (it.data.brigadier?.movers != null) it.data.brigadier?.movers?.map { it.lastName + " " + it.firstName + " " + it.lastName }
                            .toString().replace("[", "").replace("]", "") else "-"
                    binding.orderRegistrationLayout.moverName.text = movers
                    binding.orderRegistrationLayout.buttonPlaceOrder.visibility = View.GONE
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE

                    binding.orderRegistrationLayout.brigadierPhotoNameContainer.setOnClickListener { view ->
                        it.data.brigadier?.firstName
                        val mobileNumber = it.data.brigadier?.phone
                        val intent = Intent()
                        intent.action = Intent.ACTION_DIAL // Action for what intent called for
                        intent.data = Uri.parse("tel: $mobileNumber") // Data with intent respective action on intent
                        startActivity(intent)
                    }

                    binding.orderRegistrationLayout.buttonFinish.setOnClickListener { buttonFinish ->
                        val customDialogFragment = CustomDialogFragment(
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            it.data.id!!
                        )
                        customDialogFragment.arguments = Bundle().also { bundle ->
                            bundle.putString("dialogTitle", "Завершить заказ?")
                            bundle.putString("confirmOrCancel", "confirm")
                        }
                        customDialogFragment.show(childFragmentManager, "tag")
                    }

                    binding.orderRegistrationLayout.cancelOrder.setOnClickListener { cancelOrder ->
                        val customDialogFragment = CustomDialogFragment(
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            this@OrderRegistrationFragment,
                            it.data.id!!
                        )
                        customDialogFragment.arguments = Bundle().also { bundle ->
                            bundle.putString("dialogTitle", "Отменить заказ?")
                            bundle.putString("confirmOrCancel", "cancel")
                        }
                        customDialogFragment.show(childFragmentManager, "tag")
                    }

                    viewModel.cancelOrderApiCode.observe(viewLifecycleOwner) { cancel ->
                        when (cancel) {
                            is OutcomeUi.Progress -> progressBarOrderRegistration.visibility = View.VISIBLE
                            is OutcomeUi.Failure -> {
                                when {
                                    cancel.idRes > -1 -> Toast.makeText(requireContext(), getString(cancel.idRes), Toast.LENGTH_LONG).show()
                                    cancel.idRes == -2 -> {
                                        val customDialogFragment = CustomDialogFragment(
                                            this@OrderRegistrationFragment,
                                            this@OrderRegistrationFragment,
                                            this@OrderRegistrationFragment,
                                            this@OrderRegistrationFragment,
                                            it.data.id!!
                                        )
                                        customDialogFragment.arguments = Bundle().also { bundle ->
                                            bundle.putString("dialogTitle", "У вас будет списана комиссия в размере 50р, отменить заказ?")
                                            bundle.putString("confirmOrCancel", "cancel")
                                        }
                                        customDialogFragment.show(childFragmentManager, "tag")

                                    }
                                }
                            }

                            else -> {
                                findNavController().navigate(R.id.myOrdersFragment)
                            }
                        }
                    }

                    if (it.data.status == "performed" || it.data.status == "cancelled" || it.data.status == "confirmed") {
                        binding.orderRegistrationLayout.cancelOrder.visibility = View.INVISIBLE
                    }
                    if (it.data.status == "on_the_way" || it.data.status == "in_processing" || it.data.status == "cancelled" || it.data.status == "confirmed") {
                        binding.orderRegistrationLayout.buttonFinish.visibility = View.INVISIBLE
                    }
                }

                else -> {}
            }
        }
    }

//    private fun checkIfShouldEnableButton() = with(binding.orderRegistrationLayout) {
//        buttonPlaceOrder.isEnabled =
//            address.text?.isNotBlank() == true && flat.text?.isNotBlank() == true && entrance2.text?.isNotBlank() == true && floor.text?.isNotBlank() == true && title.text?.isNotBlank() == true && itemsCount.text?.isNotBlank() == true && liftsCount.text?.isNotBlank() == true && viewPagerDimensios != -1 && pagerWeight != -1 && sharedPrefsUtils.getCurrentBindingId()!!
//                .isNotEmpty() && photosByteArrayList.isNotEmpty()
//    }

    override fun confirmOrder(orderId: Int) {
        viewModel.confirmOrder(sharedPrefsUtils, orderId)
        viewModel.confirmOrderApiCode.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                else -> {
                    binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
                    findNavController().navigate(R.id.myOrdersFragment)
                }

            }
        }
    }


    override fun cancelOrder(orderId: Int) {
        viewModel.cancelOrder(sharedPrefsUtils, orderId, false)

    }

    override fun cancelOrderWithCommission(orderId: Int) {
        viewModel.cancelOrder(sharedPrefsUtils, orderId, true)
    }

    private fun installAddressGeocoderAndListenrs() {
        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        val addressList = mutableListOf<String>()
        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(it: Editable?) {
                val query = binding.orderRegistrationLayout.address.text.toString()
                binding.orderRegistrationLayout.clearAddressIV.visibility = View.VISIBLE
                binding.orderRegistrationLayout.spinnerTV.visibility = View.GONE
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        geoResults = geocoder.getFromLocationName(query, 5)
                        if (geoResults != null && geoResults?.isNotEmpty() == true) {
                            val addr = geoResults!![0]
                            withContext(Dispatchers.Main) {
                                binding.orderRegistrationLayout.spinnerTV.visibility = View.VISIBLE
                                addressAdapter.updateList(geoResults)
                            }
                        }
                    } catch (_: java.lang.Exception) {
                    }
                }
            }
        }
        binding.orderRegistrationLayout.address.addTextChangedListener(textWatcher)
        binding.orderRegistrationLayout.clearAddressIV.setOnClickListener {
            binding.orderRegistrationLayout.address.setText("")
            binding.orderRegistrationLayout.spinnerTV.visibility = View.GONE
            binding.orderRegistrationLayout.clearAddressIV.visibility = View.GONE
        }
    }


    private fun <T : Any> makeToast(toastText: String, any: Any): T {
        closeKeyBoard(
            binding.orderRegistrationLayout.address
        )
        Toast.makeText(requireContext(), toastText, Toast.LENGTH_SHORT).show()
        return any as T
    }


    private fun installPhotoContainers() {
        binding.orderRegistrationLayout.addPhotoContainerOne.apply {
            var picked = false
            if (arguments == null) {
                setOnClickListener {
                    photoCellNumber = 1
                    closeKeyBoard(it)
                    if (this.tag == null) binding.orderRegistrationLayout.bottomSheetPhotopick.visibility = View.VISIBLE
                    else startImageViewer(isRedactMode = true, appCompatImageView = this)
                }
                setOnLongClickListener {
                    if (this.tag != null && !picked) {
                        binding.orderRegistrationLayout.circleSelectCross1.visibility = View.VISIBLE
                        picked = true
                        binding.orderRegistrationLayout.circleSelectCross1.setOnClickListener { circleSelectCross ->
                            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cargo_photo))
                            binding.orderRegistrationLayout.circleSelectCross1.visibility = View.GONE
                            this.tag = null
                        }
                    } else {
                        picked = false
                        binding.orderRegistrationLayout.circleSelectCross1.visibility = View.GONE
                    }
                    true
                }
            } else {
                setOnClickListener {
                    if (photosUrlPaths[0].isEmpty()) return@setOnClickListener
                    startImageViewer(isRedactMode = false, appCompatImageView = this)
                }
            }
        }
        binding.orderRegistrationLayout.addPhotoContainerTwo.apply {
            var picked = false
            if (arguments == null) {
                setOnClickListener {
                    photoCellNumber = 2
                    closeKeyBoard(it)
                    if (this.tag == null) binding.orderRegistrationLayout.bottomSheetPhotopick.visibility = View.VISIBLE
                    else startImageViewer(isRedactMode = true, appCompatImageView = this)
                }
                setOnLongClickListener {
                    if (this.tag != null && !picked) {
                        binding.orderRegistrationLayout.circleSelectCross2.visibility = View.VISIBLE
                        picked = true
                        binding.orderRegistrationLayout.circleSelectCross2.setOnClickListener { circleSelectCross ->
                            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cargo_photo))
                            binding.orderRegistrationLayout.circleSelectCross2.visibility = View.GONE
                            this.tag = null
                        }
                    } else {
                        picked = false
                        binding.orderRegistrationLayout.circleSelectCross2.visibility = View.GONE
                    }
                    true
                }
            } else {
                setOnClickListener {
                    if (photosUrlPaths[1].isEmpty()) return@setOnClickListener
                    startImageViewer(isRedactMode = false, appCompatImageView = this)
                }
            }
        }
        binding.orderRegistrationLayout.addPhotoContainertThree.apply {
            var picked = false
            if (arguments == null) {
                setOnClickListener {
                    photoCellNumber = 3
                    closeKeyBoard(it)
                    if (this.tag == null) binding.orderRegistrationLayout.bottomSheetPhotopick.visibility = View.VISIBLE
                    else startImageViewer(isRedactMode = true, appCompatImageView = this)
                }
                setOnLongClickListener {
                    if (this.tag != null && !picked) {
                        binding.orderRegistrationLayout.circleSelectCross3.visibility = View.VISIBLE
                        picked = true
                        binding.orderRegistrationLayout.circleSelectCross3.setOnClickListener { circleSelectCross ->
                            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cargo_photo))
                            binding.orderRegistrationLayout.circleSelectCross3.visibility = View.GONE
                            this.tag = null
                        }
                    } else {
                        picked = false
                        binding.orderRegistrationLayout.circleSelectCross3.visibility = View.GONE
                    }
                    true
                }
            } else {
                setOnClickListener {
                    if (photosUrlPaths[2].isEmpty()) return@setOnClickListener
                    startImageViewer(isRedactMode = false, appCompatImageView = this)
                }
            }
        }
    }


    private fun installDimensionsAndWeightViewPagers() {
        val dimensionsList = mutableListOf(
            DimensionsOrWeights("Малогабритный", "Габариты (Д × Ш × В) не\nпревышают 0,54 × 0,39 ×\n0,39 м", false),
            DimensionsOrWeights("Среднегабаритный", "Габариты (Д × Ш × В) не\nпревышают 0,54 × 0,39 ×\n0,39 м", false),
            DimensionsOrWeights("Крупногабаритный", "Габариты (Д × Ш × В) не\nпревышают 0,54 × 0,39 ×\n0,39 м", false)
        )
        val weightsList = mutableListOf(
            DimensionsOrWeights("Легко", "(от 10 до 50 кг)", false),
            DimensionsOrWeights("Средне", "(от 50 до 100 кг)", false),
            DimensionsOrWeights("Тяжело", "(от 100 до 300 кг)", false),
            DimensionsOrWeights("Очень тяжело", "(от 300 и более)", false)
        )

        binding.orderRegistrationLayout.viewPagerDimensions.apply {
            if (arguments == null) {
                clipToPadding = false
                clipChildren = false
                offscreenPageLimit = 2
                getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
                setPadding(0, 0, (requireContext().resources.displayMetrics.widthPixels * 0.15).toInt(), 0)
                setPageTransformer(MarginPageTransformer(25))
                val adapterDimensions = ViewPagerAdapterDimensions(
                    requireContext(),
                    dimensionsList,
                    this@OrderRegistrationFragment,
                    true,
                    binding.orderRegistrationLayout.parentScrollView
                ) {
                    viewPagerDimensios = it
                    checkTextEmptiness()
                }
                adapter = adapterDimensions
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        binding.orderRegistrationLayout.parentScrollView.requestDisallowInterceptTouchEvent(true)
                        binding.orderRegistrationLayout.bottomSheetDimension.visibility = View.GONE
                    }
                })
            } else {
                val adapterDimensions = ViewPagerAdapterDimensions(
                    requireContext(),
                    dimensionsList,
                    this@OrderRegistrationFragment,
                    false,
                    binding.orderRegistrationLayout.parentScrollView
                ) {}
                adapter = adapterDimensions
                binding.orderRegistrationLayout.curtain.visibility = View.VISIBLE
            }
        }
        binding.orderRegistrationLayout.viewPagerWeight.apply {
            if (arguments == null) {
                clipToPadding = false
                clipChildren = false
                offscreenPageLimit = 2
                getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
                setPadding(0, 0, (requireContext().resources.displayMetrics.widthPixels * 0.15).toInt(), 0)
                setPageTransformer(MarginPageTransformer(25))
                val adapterWeight =
                    ViewPagerAdapterWeight(
                        requireContext(),
                        weightsList,
                        this@OrderRegistrationFragment,
                        true,
                        binding.orderRegistrationLayout.parentScrollView
                    ) {
                        pagerWeight = it
                        checkTextEmptiness()
                    }
                adapter = adapterWeight
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        binding.orderRegistrationLayout.bottomSheetWeight.visibility = View.GONE
                    }
                })
            } else {
                val adapterWeight =
                    ViewPagerAdapterWeight(
                        requireContext(),
                        weightsList,
                        this@OrderRegistrationFragment,
                        false,
                        binding.orderRegistrationLayout.parentScrollView
                    ) {

                    }
                adapter = adapterWeight
                binding.orderRegistrationLayout.curtain2.visibility = View.VISIBLE
            }
        }
    }

    fun interface DimensionPicked {
        fun onDimensionPicked(i: Int)
    }

    fun interface WeightPicked {
        fun onWeightPicked(i: Int)
    }

    class ViewPagerAdapterDimensions(
        private val context: Context,
        private val mutableList: MutableList<DimensionsOrWeights>,
        private val dimensClickListener: DimensClickListener,
        private val needArrow: Boolean = true,
        private val scrollView: NestedScrollView,
        private val dimensionPicked: DimensionPicked
    ) :
        RecyclerView.Adapter<ViewPagerAdapterDimensions.PagerVH>() {
        private lateinit var binding: ViewpagerDimensionsItemBinding

        override fun getItemCount() = mutableList.size
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
            binding = ViewpagerDimensionsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return PagerVH(binding)
        }

        override fun onBindViewHolder(holder: PagerVH, position: Int) {
            if (!needArrow) holder.dimensionsArrowPager.visibility = View.GONE
            val onePagerEntityDimension = mutableList[position]
            if (onePagerEntityDimension.pressed) {
                holder.cardContainerDimensionMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                holder.dimensionsIconCube.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_dimensions_pressed))
                holder.dimensionsViewPagerName.setTextColor(ContextCompat.getColor(context, R.color.screen_title_color))
                holder.dimensionsViewPagerDiscription.setTextColor(ContextCompat.getColor(context, R.color.screen_title_color))
                dimensClickListener.onDimensClick("press")
            } else {
                holder.cardContainerDimensionMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.main_green_color))
                holder.dimensionsIconCube.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_dimensions))
                holder.dimensionsViewPagerName.setTextColor(ContextCompat.getColor(context, R.color.white))
                holder.dimensionsViewPagerDiscription.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
            holder.dimensionsViewPagerName.text = onePagerEntityDimension.name
            holder.dimensionsViewPagerDiscription.text = onePagerEntityDimension.discription

//            holder.cardContainerDimension.setOnTouchListener { view, motionEvent ->
//                when (motionEvent.action) {
//                    MotionEvent.ACTION_MOVE -> scrollView.requestDisallowInterceptTouchEvent(true)
//                }
//                true
//            }
            holder.cardContainerDimension.setOnClickListener {
                unpressAll()
                onePagerEntityDimension.pressed = !onePagerEntityDimension.pressed
                for (i in 0 until mutableList.size) {
                    if (i == position) {
                        dimensionPicked.onDimensionPicked(i)
                        notifyItemChanged(i)
                    }
                }
            }

        }

        inner class PagerVH(binding: ViewpagerDimensionsItemBinding) : RecyclerView.ViewHolder(
            binding.root
        ) {
            var cardContainerDimension = binding.cardContainerDimension
            var cardContainerDimensionMain = binding.cardContainerDimensionMain
            var dimensionsIconCube = binding.dimensionsIconCube
            var dimensionsViewPagerName = binding.dimensionsViewPagerName
            var dimensionsViewPagerDiscription = binding.dimensionsViewPagerDiscription
            var dimensionsArrowPager = binding.dimensionsArrowPager
        }

        private fun unpressAll() {
            for (i in 0 until mutableList.size) {
                if (mutableList[i].pressed) {
                    mutableList[i].pressed = false
                    notifyItemChanged(i)
                }
            }
        }

        fun pressCurrent(position: Int) {
            mutableList[position].pressed = true
            notifyItemChanged(position)
        }
    }


    class ViewPagerAdapterWeight(
        private val context: Context,
        private val mutableList: MutableList<DimensionsOrWeights>,
        private val weightClickListener: WeightClickListener,
        private val needArrow: Boolean = true,
        private val scrollView: NestedScrollView,
        private val weigthPicked: WeightPicked
    ) :
        RecyclerView.Adapter<ViewPagerAdapterWeight.PagerVH>() {
        private lateinit var binding: ViewpagerWeightItemBinding
        override fun getItemCount() = mutableList.size
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
            binding = ViewpagerWeightItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return PagerVH(binding)
        }

        override fun onBindViewHolder(holder: PagerVH, position: Int) {
            if (!needArrow) holder.weightArrowPager.visibility = View.GONE
            val onePagerEntityWeight = mutableList[position]
            if (onePagerEntityWeight.pressed) {
                holder.cardContainerWeightMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                holder.weightViewPagerName.setTextColor(ContextCompat.getColor(context, R.color.screen_title_color))
                holder.weightViewPagerDiscription.setTextColor(ContextCompat.getColor(context, R.color.screen_title_color))
                weightClickListener.onWeightClick("press")
            } else {
                holder.cardContainerWeightMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.main_green_color))
                holder.weightViewPagerName.setTextColor(ContextCompat.getColor(context, R.color.white))
                holder.weightViewPagerDiscription.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
            holder.weightViewPagerName.text = onePagerEntityWeight.name
            holder.weightViewPagerDiscription.text = onePagerEntityWeight.discription
            holder.cardContainerWeight.setOnClickListener {
                unpressAll()
                onePagerEntityWeight.pressed = !onePagerEntityWeight.pressed
                for (i in 0 until mutableList.size) {
                    if (i == position) {
                        weigthPicked.onWeightPicked(i)
                        notifyItemChanged(i)
                    }
                }
            }
        }

        inner class PagerVH(binding: ViewpagerWeightItemBinding) : RecyclerView.ViewHolder(
            binding.root
        ) {
            var weightViewPagerName = binding.weightViewPagerName
            var weightViewPagerDiscription = binding.weightViewPagerDiscription
            var cardContainerWeight = binding.cardContainerWeight
            var cardContainerWeightMain = binding.cardContainerWeightMain
            var weightArrowPager = binding.weightArrowPager
        }

        private fun unpressAll() {
            for (i in 0 until mutableList.size) {
                if (mutableList[i].pressed) {
                    mutableList[i].pressed = false
                    notifyItemChanged(i)
                }
            }
        }

        fun pressCurrent(position: Int) {
            mutableList[position].pressed = true
            notifyItemChanged(position)
        }
    }


    override fun onDimensClick(s: String) {
        onDimensClickPress = s
        closeKeyBoard(
            binding.orderRegistrationLayout.address
        )
    }


    override fun onWeightClick(s: String) {
        onWeightClickPress = s
        closeKeyBoard(
            binding.orderRegistrationLayout.address
        )
    }


    private fun installSideMenu() {
        val sideFrameLayout = SideMenuFragment()
        childFragmentManager.beginTransaction().replace(R.id.motionLayoutContainer, sideFrameLayout).commit()
        binding.orderRegistrationLayout.menuVertical.setOnClickListener {
            closeKeyBoard(it)
            val motionLayoutContainer = binding.orderRegistrationLayout.motionLayoutContainer
            with(sideFrameLayout.motionLayout) {
                val parentLayoutParams = motionLayoutContainer.layoutParams as FrameLayout.LayoutParams
                if (this?.progress == 0.0F) {
                    parentLayoutParams.marginStart = 0
                    motionLayoutContainer.layoutParams = parentLayoutParams
                    this.transitionToEnd()
                } else {
                    this?.transitionToStart()
                    parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                    motionLayoutContainer.layoutParams = parentLayoutParams
                }
                this?.addTransitionListener(object : MotionLayout.TransitionListener {
                    override fun onTransitionStarted(motionLayout: MotionLayout?, startId: Int, endId: Int) {}
                    override fun onTransitionChange(motionLayout: MotionLayout?, startId: Int, endId: Int, progress: Float) {}
                    override fun onTransitionTrigger(motionLayout: MotionLayout?, triggerId: Int, positive: Boolean, progress: Float) {}
                    override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                        if (this@with.progress == 0.0F) {
                            parentLayoutParams.marginStart = resources.displayMetrics.widthPixels - 2
                            motionLayoutContainer.layoutParams = parentLayoutParams
                        }
                    }
                })
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun installBottomSheetsDrugAnimation() {
        val density = requireContext().resources.displayMetrics.density

        var firstTouchPosition = 0
        val layoutParams = binding.orderRegistrationLayout.bottomSheetDimension.layoutParams as FrameLayout.LayoutParams
        binding.orderRegistrationLayout.bottomSheetDimension.setOnTouchListener { v, event ->
            val delta = firstTouchPosition - event.rawY.toInt()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> firstTouchPosition = event.rawY.toInt()
                MotionEvent.ACTION_MOVE -> {
                    if (delta < 0) {
                        layoutParams.bottomMargin = (delta / density - 40).toInt()
                        binding.orderRegistrationLayout.bottomSheetDimension.layoutParams = layoutParams
                        if (delta < -300) {
                            layoutParams.bottomMargin = (-25 / density).toInt()
                            binding.orderRegistrationLayout.bottomSheetDimension.layoutParams = layoutParams
                            binding.orderRegistrationLayout.bottomSheetDimension.visibility = View.GONE
                        }
                    }
                }

                MotionEvent.ACTION_UP -> {
                    if (delta > -300) {
                        layoutParams.bottomMargin = (-25 / density).toInt()
                        binding.orderRegistrationLayout.bottomSheetDimension.layoutParams = layoutParams
                    }
                }
            }
            true
        }

        var firstTouchPosition2 = 0
        val layoutParams2 = binding.orderRegistrationLayout.bottomSheetWeight.layoutParams as FrameLayout.LayoutParams
        binding.orderRegistrationLayout.bottomSheetWeight.setOnTouchListener { v, event ->
            val delta = firstTouchPosition2 - event.rawY.toInt()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> firstTouchPosition2 = event.rawY.toInt()
                MotionEvent.ACTION_MOVE -> {
                    if (delta < 0) {
                        layoutParams2.bottomMargin = (delta / density - 40).toInt()
                        binding.orderRegistrationLayout.bottomSheetWeight.layoutParams = layoutParams2
                        if (delta < -300) {
                            layoutParams2.bottomMargin = (-25 / density).toInt()
                            binding.orderRegistrationLayout.bottomSheetWeight.layoutParams = layoutParams2
                            binding.orderRegistrationLayout.bottomSheetWeight.visibility = View.GONE
                        }
                    }
                }

                MotionEvent.ACTION_UP -> {
                    if (delta > -300) {
                        layoutParams2.bottomMargin = (-25 / density).toInt()
                        binding.orderRegistrationLayout.bottomSheetWeight.layoutParams = layoutParams2
                    }
                }
            }
            true
        }

        var firstTouchPosition3 = 0
        val layoutParams3 = binding.orderRegistrationLayout.bottomSheetPhotopick.layoutParams as FrameLayout.LayoutParams
        binding.orderRegistrationLayout.bottomSheetPhotopick.setOnTouchListener { v, event ->
            val delta = firstTouchPosition3 - event.rawY.toInt()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> firstTouchPosition3 = event.rawY.toInt()
                MotionEvent.ACTION_MOVE -> {
                    if (delta < 0) {
                        layoutParams3.bottomMargin = (delta / density - 40).toInt()
                        binding.orderRegistrationLayout.bottomSheetPhotopick.layoutParams = layoutParams3
                        if (delta < -300) {
                            layoutParams3.bottomMargin = (-25 / density).toInt()
                            binding.orderRegistrationLayout.bottomSheetPhotopick.layoutParams = layoutParams3
                            binding.orderRegistrationLayout.bottomSheetPhotopick.visibility = View.GONE
                        }
                    }
                }

                MotionEvent.ACTION_UP -> {
                    if (delta > -300) {
                        layoutParams3.bottomMargin = (-25 / density).toInt()
                        binding.orderRegistrationLayout.bottomSheetPhotopick.layoutParams = layoutParams3
                    }
                }
            }
            true
        }

    }


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val photoContainer = when (photoCellNumber) {
                1 -> binding.orderRegistrationLayout.addPhotoContainerOne
                2 -> binding.orderRegistrationLayout.addPhotoContainerTwo
                3 -> binding.orderRegistrationLayout.addPhotoContainertThree
                else -> return
            }
            if (data?.data != null) {
                val uri = data.data
                val bytes = requireContext().contentResolver.openInputStream(uri!!)?.readBytes()!!
                if (bytes.size > 15_000_000) Toast.makeText(
                    App.appContext,
                    "Размер фото превышает 15 Мб, выберите фото меньшего размера!",
                    Toast.LENGTH_LONG
                ).show().also { return }
                photosByteArrayList.add(bytes)
                Glide.with(requireContext())
                    .load(uri)
                    .apply(RequestOptions().centerCrop())
                    .apply(RequestOptions().override(1000, 1000))
                    .into(photoContainer)
                photoContainer.tag = uri.toString()
                checkTextEmptiness()
            } else {
//                val bitmap = data?.extras?.get("data") as Bitmap
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ByteArrayOutputStream())
//                val path = Images.Media.insertImage(requireContext().contentResolver, bitmap, "IMG_" + Calendar.getInstance().time, null)
//                val uri = Uri.parse(path)
                val bytes = imageUri?.let { requireContext().contentResolver.openInputStream(it)?.readBytes() }!!
                if (bytes.size > 15_000_000) Toast.makeText(
                    App.appContext,
                    "Размер фото превышает 15 Мб, выберите фото меньшего размера!",
                    Toast.LENGTH_LONG
                ).show().also { return }
                photosByteArrayList.add(bytes)
                Glide.with(requireContext())
                    .load(imageUri)
                    .apply(RequestOptions().centerCrop())
                    .apply(RequestOptions().override(1000, 1000))
                    .into(photoContainer)
                photoContainer.tag = imageUri.toString()
            }
            binding.orderRegistrationLayout.bottomSheetPhotopick.visibility = View.GONE
        }
    }


    data class DimensionsOrWeights(val name: String, val discription: String, var pressed: Boolean)


    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    private fun disableEditText(editText: EditText) {
        editText.isFocusable = false;
        editText.isEnabled = false;
        editText.isCursorVisible = false;
        editText.keyListener = null;
//        editText.setBackgroundColor(Color.TRANSPARENT);
    }


    private fun startImageViewer(isRedactMode: Boolean, appCompatImageView: AppCompatImageView) {
        val imageViewerFragment = ImageViewerFragment()
        imageViewerFragment.arguments =
            Bundle().also { it.putStringArrayList("button_pressed_key", arrayListOf(appCompatImageView.tag.toString())) }
        imageViewerFragment.show(childFragmentManager, "tag")
    }


    private fun installTextFieldsBehavior() {
        binding.orderRegistrationLayout.address.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.flat.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.entrance2.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.floor.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.title.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.itemsCount.addTextChangedListener { checkTextEmptiness() }
        binding.orderRegistrationLayout.liftsCount.addTextChangedListener {
            checkTextEmptiness()
            if (binding.orderRegistrationLayout.liftsCount.text?.isNotEmpty() == true && binding.orderRegistrationLayout.itemsCount.text?.isNotEmpty() == true) {
                if (binding.orderRegistrationLayout.liftsCount.text.toString()
                        .toInt() > binding.orderRegistrationLayout.itemsCount.text.toString().toInt()
                )
                    Toast.makeText(App.appContext, "Количество подъемов больше количества грузов?", Toast.LENGTH_LONG).show()
            }
        }
    }


    private fun checkTextEmptiness() {
        if (binding.orderRegistrationLayout.address.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.flat.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.entrance2.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.floor.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.title.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.itemsCount.text?.isNotEmpty() == true
            && binding.orderRegistrationLayout.liftsCount.text?.isNotEmpty() == true
            && viewPagerDimensios != -1
            && pagerWeight != -1
            && sharedPrefsUtils.getCurrentBindingId()?.isNotEmpty() == true
        ) {
            binding.orderRegistrationLayout.buttonPlaceOrder.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            binding.orderRegistrationLayout.buttonPlaceOrder.isEnabled = true
        } else {
            binding.orderRegistrationLayout.buttonPlaceOrder.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
            binding.orderRegistrationLayout.buttonPlaceOrder.isEnabled = false
        }
        checkExitBehaviour()
        if (binding.orderRegistrationLayout.address.text.toString().length > 0)
            binding.orderRegistrationLayout.address.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.address.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.flat.text.toString().length > 0)
            binding.orderRegistrationLayout.flat.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.flat.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.entrance2.text.toString().length > 0)
            binding.orderRegistrationLayout.entrance2.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.entrance2.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.floor.text.toString().length > 0)
            binding.orderRegistrationLayout.floor.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.floor.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.title.text.toString().length > 0)
            binding.orderRegistrationLayout.title.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.title.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.itemsCount.text.toString().length > 0)
            binding.orderRegistrationLayout.itemsCount.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.itemsCount.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.liftsCount.text.toString().length > 0)
            binding.orderRegistrationLayout.liftsCount.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.liftsCount.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.orderRegistrationLayout.comment.text.toString().length > 0)
            binding.orderRegistrationLayout.comment.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.orderRegistrationLayout.comment.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)

    }


    private fun checkExitBehaviour() {
        if (arguments != null) return
        if (binding.orderRegistrationLayout.address.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.flat.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.entrance2.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.floor.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.title.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.itemsCount.text?.isNotEmpty() == true
            || binding.orderRegistrationLayout.liftsCount.text?.isNotEmpty() == true
        ) {
            binding.orderRegistrationLayout.backArrow.setOnClickListener {
                exitAttempts++
                if (exitAttempts > 1) {
                    lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
                    exitAttempts = 0
                } else Toast.makeText(
                    requireContext(),
                    "При выходе из раздела все введенные данные будут потеряны. Для выхода нажмите еще раз",
                    Toast.LENGTH_LONG
                ).show()
            }
            requireActivity().onBackPressedDispatcher.addCallback {
                exitAttempts++
                if (exitAttempts > 1) {
                    lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
                    exitAttempts = 0
                } else Toast.makeText(
                    requireContext(),
                    "При выходе из раздела все введенные данные будут потеряны. Для выхода нажмите еще раз",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun userDismissed() {
        binding.orderRegistrationLayout.progressBarOrderRegistration.visibility = View.GONE
    }


}