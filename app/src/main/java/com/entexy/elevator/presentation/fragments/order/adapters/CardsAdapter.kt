package com.entexy.elevator.presentation.fragments.order.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.ListAdapter
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.RecyclerItemCardInactiveBinding
import com.entexy.elevator.presentation.fragments.order.adapters.holders.InactiveCardViewHolder
import com.entexy.elevator.presentation.fragments.order.adapters.item.CardItem

class CardsAdapter(private val sharedPrefsUtils: SharedPrefsUtils) : ListAdapter<CardItem, InactiveCardViewHolder>(CardsItemDiffUtil()) {

    private var currentPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InactiveCardViewHolder = InactiveCardViewHolder(
        RecyclerItemCardInactiveBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: InactiveCardViewHolder, position: Int) = with(holder.binding) {
        val item = getItem(position)
        tvCardNumber.text = item.number
        when (item.paymentSystem) {
            "MASTERCARD" -> {
                ivPaymentSystem.setImageDrawable(ResourcesCompat.getDrawable(root.resources, R.drawable.ic_master_card, null))
                tvPaymentSystemName.text = "Mastercard"
            }

            else -> {
                ivPaymentSystem.setImageDrawable(ResourcesCompat.getDrawable(root.resources, R.drawable.ic_visa, null))
                tvPaymentSystemName.text = "Visa"
            }
        }

        if (item.isCurrent) {
            ivPaymentSystem.alpha = 1f
            tvPaymentSystemName.alpha = 1f
            tvPaymentSystemName.setTextColor(root.resources.getColor(R.color.black_text, null))
            tvCardNumber.alpha = 1f
            tvCardNumber.setTextColor(root.resources.getColor(R.color.black_text, null))
            ivDeleteCard.alpha = 1f
            root.background = ResourcesCompat.getDrawable(root.resources, R.drawable.recycler_item_card_active_background, null)
            currentPosition = holder.adapterPosition
        }
        root.setOnClickListener {
            getItem(currentPosition).isCurrent = false
            item.isCurrent = true
            notifyItemChanged(currentPosition)
            notifyItemChanged(holder.adapterPosition)
            currentPosition = holder.adapterPosition
            sharedPrefsUtils.setCurrentBindingId(item.bindingId)
        }
    }

}