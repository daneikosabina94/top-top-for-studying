package com.entexy.elevator.presentation.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.ProfileUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val profileUseCase: ProfileUseCase) : ViewModel() {
    private val _logoutStatus = MutableLiveData<OutcomeUi<Unit>>()
    val logoutStatus: LiveData<OutcomeUi<Unit>> = _logoutStatus

    fun logout(sharedPrefsUtils: SharedPrefsUtils) = viewModelScope.launch(Dispatchers.IO) {
        profileUseCase.logout(sharedPrefsUtils).collect {
            _logoutStatus.postValue(it)
        }
    }
}