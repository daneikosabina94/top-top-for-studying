package com.entexy.elevator.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.App
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentSideMenuBinding

class SideMenuFragment : Fragment() {

    private lateinit var binding: FragmentSideMenuBinding
    var motionLayout: MotionLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSideMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        motionLayout = binding.motionLayout

        val sharedPrefsUtils = SharedPrefsUtils(requireContext())
        binding.userName.text = sharedPrefsUtils.getUserName().ifEmpty { getString(R.string.profile) }

        binding.userName.setOnClickListener {
            binding.progressBarSideMenu.visibility = View.VISIBLE
            if (findNavController().currentDestination?.id != R.id.myProfileFragment)
                lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.myProfileFragment)}
            else Toast.makeText(requireContext(), "Вы уже на этом экране", Toast.LENGTH_SHORT).show().apply { binding.progressBarSideMenu.visibility = View.GONE }
        }
        binding.myOrders.setOnClickListener {
            binding.progressBarSideMenu.visibility = View.VISIBLE
            if (findNavController().currentDestination?.id != R.id.myOrdersFragment)
                lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.myOrdersFragment)}
            else Toast.makeText(requireContext(), "Вы уже на этом экране", Toast.LENGTH_SHORT).show().apply { binding.progressBarSideMenu.visibility = View.GONE }
        }
        binding.orderRegistration.setOnClickListener {
            binding.progressBarSideMenu.visibility = View.VISIBLE
            if (findNavController().currentDestination?.id != R.id.orderRegistrationFragment)
                lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.orderRegistrationFragment)}
            else Toast.makeText(requireContext(), "Вы уже на этом экране", Toast.LENGTH_SHORT).show().apply { binding.progressBarSideMenu.visibility = View.GONE }
        }
        binding.support.setOnClickListener {
            binding.progressBarSideMenu.visibility = View.VISIBLE
            if (findNavController().currentDestination?.id != R.id.mySupportFragment)
                lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.mySupportFragment)}
            else Toast.makeText(requireContext(), "Вы уже на этом экране", Toast.LENGTH_SHORT).show().apply { binding.progressBarSideMenu.visibility = View.GONE }
        }

    }


}