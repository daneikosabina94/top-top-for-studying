package com.entexy.elevator.presentation.activity

interface LogoutCallback {
    fun logout()
}