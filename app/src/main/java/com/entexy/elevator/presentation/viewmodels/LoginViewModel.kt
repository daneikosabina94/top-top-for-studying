package com.entexy.elevator.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.uscases.LoginUseCase
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(val loginUseCase: LoginUseCase) : ViewModel() {

    val accessAndRefreshToken = MutableLiveData<OutcomeUi<MutableMap<String, String>>>()

    fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String) = viewModelScope.launch(Dispatchers.IO) {
        loginUseCase.login(sharedPrefsUtils = sharedPrefsUtils, tel = tel, password = password).collect {
            accessAndRefreshToken.postValue(it)
        }
    }


}