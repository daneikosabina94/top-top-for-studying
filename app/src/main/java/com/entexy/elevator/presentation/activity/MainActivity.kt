package com.entexy.elevator.presentation.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.ActivityMainBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.OrderRegistrationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModel<OrderRegistrationViewModel>()
    private val mainViewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.decorView.systemUiVisibility = (SYSTEM_UI_FLAG_LAYOUT_STABLE or SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        WindowCompat.getInsetsController(window, window.decorView).isAppearanceLightStatusBars = true
        val navController = findNavController(R.id.nav_host_fragment_content_main)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        val sharedPrefsUtils = SharedPrefsUtils(this)


        requestPermissions()

        setNavigationGraph(navController, sharedPrefsUtils)

        mainViewModel.logoutStatus.observe(this) {
            Log.d("Check this logout", "$it")
            when (it) {
                is OutcomeUi.Progress -> {}
                is OutcomeUi.Failure -> {
                }

                else -> {

                }
            }
        }

        logoutCallback = object : LogoutCallback {
            override fun logout() {
                sharedPrefsUtils.setAccessToken("")
                sharedPrefsUtils.setRemember(false)
                sharedPrefsUtils.setUserPhone("")
                sharedPrefsUtils.setUserName("")
                lifecycleScope.launchWhenResumed { navController.navigate(R.id.loginFragment) }
            }
        }

    }


    private fun setNavigationGraph(navController: NavController, sharedPrefsUtils: SharedPrefsUtils) {
        val navGraph = navController.navInflater.inflate(R.navigation.nav_graph)
        if (sharedPrefsUtils.getFirstStart()) {
            navGraph.setStartDestination(R.id.startScreen1Fragment)
            sharedPrefsUtils.setFirstStart(false)
        } else if (sharedPrefsUtils.getAccessToken().isEmpty() || !sharedPrefsUtils.getRemember()) {
            navGraph.setStartDestination(R.id.loginFragment)
        } else {
            navGraph.setStartDestination(R.id.myOrdersFragment)
        }
        navController.graph = navGraph
    }


    private fun requestPermissions() {

        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
//            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
//            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                    Manifest.permission.CAMERA,
//                    Manifest.permission.READ_PHONE_STATE,
//                    Manifest.permission.READ_CONTACTS
                ), 1111
            )
        } else {
            //
        }

    }

    companion object {
        var logoutCallback: LogoutCallback? = null
    }

}

