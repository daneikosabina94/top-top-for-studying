package com.entexy.elevator.presentation.fragments.registration

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.databinding.FragmentRegistrationBinding
import com.entexy.elevator.presentation.helpers.OutcomeUi
import com.entexy.elevator.presentation.viewmodels.RegistrationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class RegistrationFragment : Fragment() {

    private lateinit var binding: FragmentRegistrationBinding
    private val viewModel by viewModel<RegistrationViewModel>()
    private val args by lazy {
        RegistrationFragmentArgs.fromBundle(requireArguments())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegistrationBinding.inflate(inflater, container, false)
        return binding.root
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack(R.id.loginFragment, false)
            }
        })
        val sharedPrefsUtils = SharedPrefsUtils(requireContext())
        val otp = args.otp
        val tel = args.phoneNumber
        binding.phoneNumberContainer.visibility = View.GONE
        binding.phoneNumber.visibility = View.GONE
        binding.buttonRegistration.setOnClickListener {
            if (binding.firstName.text.toString().isNotBlank()) {
                if (binding.password1.text.toString() == binding.password2.text.toString()) {
                    viewModel.register(
                        sharedPrefsUtils = sharedPrefsUtils, first_name = binding.firstName.text.toString(),
                        phone_number = tel, password1 = binding.password1.text.toString(),
                        password2 = binding.password2.text.toString(), otp = otp
                    )
                } else Toast.makeText(requireContext(), getString(R.string.error_fields_dont_match), Toast.LENGTH_LONG).show()
            } else Toast.makeText(requireContext(), getString(R.string.error_empty_name), Toast.LENGTH_LONG).show()
        }
        binding.entrance.setOnClickListener {
            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.action_registrationFragment_to_loginFragment) }
        }
        binding.superRootRegistration.setOnClickListener { closeKeyBoard(it) }
        viewModel.accessAndRefreshToken.observe(viewLifecycleOwner) {
            when (it) {
                is OutcomeUi.Progress -> binding.progressBarRegistration.visibility = View.VISIBLE
                is OutcomeUi.Failure -> {
                    binding.progressBarRegistration.visibility = View.GONE
                    if (it.idRes != -1) Toast.makeText(requireContext(), getString(it.idRes), Toast.LENGTH_LONG).show()
                }

                is OutcomeUi.Success -> {
                    if (it.data.keys.first().isNotEmpty() && it.data.values.first().isNotEmpty()) {
                        sharedPrefsUtils.setUserName(binding.firstName.text.toString())
                        if (binding.checkboxRegistration.isChecked) sharedPrefsUtils.setRemember(true)
                        if (findNavController().currentDestination?.id != R.id.action_registrationFragment_to_myOrdersFragment) {
                            findNavController().popBackStack(R.id.myOrdersFragment, true)
                            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
                        } else {
                            lifecycleScope.launchWhenResumed { findNavController().navigate(R.id.myOrdersFragment) }
                        }
                    }
                    binding.progressBarRegistration.visibility = View.GONE
                }

                else -> {}
            }
        }
        textFieldsBehavior()
    }


    private fun textFieldsBehavior() {
        if (arguments == null) {
            binding.phoneNumber.addTextChangedListener {
                if (binding.phoneNumber.text?.isNotEmpty() == true) {
                    binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
                    binding.buttonRegistration.isEnabled = true
                    binding.buttonRegistration.setTextColor(Color.WHITE)
                    if (binding.phoneNumber.text.toString().length > 10) binding.phoneNumber.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
                } else {
                    binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
                    binding.buttonRegistration.isEnabled = false
                    binding.buttonRegistration.setTextColor(Color.GRAY)
                    binding.phoneNumber.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
                }
                if (binding.phoneNumber.text.toString().length > 7) binding.phoneNumber.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
                else binding.phoneNumber.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
            }
        } else {
            binding.firstName.addTextChangedListener { checkEmptiness(binding.firstName) }
            binding.password1.addTextChangedListener { checkEmptiness(binding.password1) }
            binding.password2.addTextChangedListener { checkEmptiness(binding.password2) }
        }
    }

    private fun checkEmptiness(editText: AppCompatEditText) {
        if (binding.firstName.text?.isNotBlank() == true && binding.password1.text?.isNotBlank() == true && binding.password2.text?.isNotBlank() == true) {
            binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_active)
            binding.buttonRegistration.isActivated = true
            binding.buttonRegistration.isEnabled = true
            binding.buttonRegistration.setTextColor(Color.WHITE)
            if (binding.phoneNumber.text.toString().length > 7) editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        } else {
            binding.buttonRegistration.background = ContextCompat.getDrawable(requireContext(), R.drawable.bottom_button_unactive)
            binding.buttonRegistration.isActivated = false
            editText.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
            binding.buttonRegistration.setTextColor(Color.GRAY)
        }
        if (binding.password1.text.toString().length > 7) binding.password1.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.password1.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
        if (binding.password2.text.toString().length > 7) binding.password2.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.textview_filled)
        else binding.password2.background = ContextCompat.getDrawable(requireContext(), R.drawable.textview_empty)
    }

    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


}