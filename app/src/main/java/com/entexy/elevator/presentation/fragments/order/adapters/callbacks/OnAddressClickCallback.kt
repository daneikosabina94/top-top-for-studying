package com.entexy.elevator.presentation.fragments.order.adapters.callbacks

interface OnAddressClickCallback {
    fun onAddressClick(address: String)
}