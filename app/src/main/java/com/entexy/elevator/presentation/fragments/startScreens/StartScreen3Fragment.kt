package com.entexy.meetformeet.presentation.startScreens

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.databinding.FragmentStartScreen3Binding


class StartScreen3Fragment : Fragment() {

    private lateinit var binding: FragmentStartScreen3Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentStartScreen3Binding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var firstTouchPosition = 0
        binding.startFrag3Back.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> firstTouchPosition = event.rawX.toInt()
                MotionEvent.ACTION_UP -> {
                    if ((event.rawX.toInt() - firstTouchPosition) < 50)
                        lifecycleScope.launchWhenResumed {
                            findNavController().navigate(StartScreen3FragmentDirections.actionStartScreen3FragmentToLoginFragment())
                        }
                }

                MotionEvent.ACTION_MOVE -> {
                    if ((event.rawX.toInt() - firstTouchPosition) > 300)
                        activity?.onBackPressed()
                }
            }
            true
        }

    }


}