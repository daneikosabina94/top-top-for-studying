package com.entexy.elevator.presentation.helpers

import android.app.ActionBar
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import com.entexy.elevator.databinding.FragmentDialogusBinding
import com.entexy.elevator.presentation.CancelOrderListener
import com.entexy.elevator.presentation.CancelOrderWithCommission
import com.entexy.elevator.presentation.ConfirmOrderListener
import com.entexy.elevator.presentation.UserDismissed


class CustomDialogFragment(
    private val confirmOrderListener: ConfirmOrderListener,
    private val cancelOrderListener: CancelOrderListener,
    private val cancelOrderWithCommission: CancelOrderWithCommission,
    private val userDismissedListener: UserDismissed,
    private val orderId: Int
) : DialogFragment() {
    private lateinit var binding: FragmentDialogusBinding

    override fun onResume() {
        super.onResume()

        dialog?.window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setGravity(Gravity.BOTTOM)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        closeKeyBoard(binding.root)
        binding.secondtext.setOnClickListener {
            this.dialog?.dismiss()
        }

        Log.d("!!!", "$arguments")
        if (arguments != null) {
            if (requireArguments().getString("dialogTitle") == "У вас будет списана комиссия в размере 50р, отменить заказ?") {
                binding.firsttext.text = requireArguments().getString("dialogTitle")
                binding.thirdtext.setOnClickListener {
                    cancelOrderWithCommission.cancelOrderWithCommission(orderId = orderId)
                    this.dialog?.dismiss()
                }
                binding.secondtext.setOnClickListener {
                    userDismissedListener.userDismissed()
                    this.dialog?.dismiss()
                }
            } else {
                binding.firsttext.text = requireArguments().getString("dialogTitle", "Завершить?")
                if (requireArguments().getString("confirmOrCancel", "") == "confirm") {
                    binding.thirdtext.setOnClickListener {
                        confirmOrderListener.confirmOrder(orderId = orderId)
                        this.dialog?.dismiss()
                    }
                } else {
                    binding.thirdtext.setOnClickListener {
                        cancelOrderListener.cancelOrder(orderId = orderId)
                        this.dialog?.dismiss()
                    }
                }
            }

        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = FragmentDialogusBinding.inflate(LayoutInflater.from(requireContext()))
        return AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .create()
    }


    private fun closeKeyBoard(view: View) {
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}




