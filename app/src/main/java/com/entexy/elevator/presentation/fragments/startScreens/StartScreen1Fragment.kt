package com.entexy.meetformeet.presentation.startScreens

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.entexy.elevator.R
import com.entexy.elevator.databinding.FragmentStartScreen1Binding


class StartScreen1Fragment : Fragment() {

    private lateinit var binding: FragmentStartScreen1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { }
        val window  = requireActivity().window
        WindowInsetsControllerCompat(window, window!!.decorView).isAppearanceLightStatusBars = true
        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.transparent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentStartScreen1Binding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var firstTouchPosition = 0
        binding.startFrag1Back.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN ->  firstTouchPosition = event.rawX.toInt()
                MotionEvent.ACTION_UP -> {
                    if ((event.rawX.toInt() - firstTouchPosition) < 50)
                        lifecycleScope.launchWhenResumed {findNavController().navigate(R.id.action_startScreen1Fragment_to_startScreen2Fragment)}
                }
            }
            true
        }
    }



}