package com.entexy.elevator.presentation.fragments.order.adapters.item

data class CardItem(
    val number: String,
    val paymentSystem: String,
    val bindingId: String,
    var isCurrent: Boolean
)
