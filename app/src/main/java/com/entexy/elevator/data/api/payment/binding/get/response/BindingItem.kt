package com.entexy.elevator.data.api.payment.binding.get.response

import com.google.gson.annotations.SerializedName

data class BindingItem(
    @SerializedName("binding_id") val bindingId: String,
    @SerializedName("masked_pan") val cardNumber: String,
    @SerializedName("payment_system") val paymentSystem: String
)