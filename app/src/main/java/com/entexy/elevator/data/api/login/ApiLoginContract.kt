package com.entexy.elevator.data.api.login

import com.entexy.elevator.data.api.JvtCredentials
import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiLoginContract {
    fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String): JvtCredentials
}