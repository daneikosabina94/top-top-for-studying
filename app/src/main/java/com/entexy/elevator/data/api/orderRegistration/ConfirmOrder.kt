package com.entexy.elevator.data.api.myOrders

import com.google.gson.annotations.SerializedName

data class ConfirmOrder(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("performed_at")
    var performed_at: String? = null,
    @SerializedName("status")
    var status: String? = null

)