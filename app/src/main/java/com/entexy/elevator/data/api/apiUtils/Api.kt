package com.entexy.elevator.data.api.apiUtils

import com.entexy.elevator.data.api.JvtCredentials
import com.entexy.elevator.data.api.Order
import com.entexy.elevator.data.api.Photos
import com.entexy.elevator.data.api.myOrders.CancelOrder
import com.entexy.elevator.data.api.myOrders.ConfirmOrder
import com.entexy.elevator.data.api.myOrders.OrdersApi
import com.entexy.elevator.data.api.payment.binding.create.response.CreateBindingResponse
import com.entexy.elevator.data.api.payment.binding.get.response.GetBindingsResponse
import com.entexy.elevator.data.api.payment.binding.verify.response.VerifyBindingResponse
import com.entexy.elevator.data.api.profile.Profile
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface Api {

    //register 1 step
    @POST("phone_number/verify/")
    fun verifyRegistrationPhone(@Header("role") role: String, @Body params: RequestBody): Call<String>

    //register 3 step
    @Headers("Content-Type: application/json")
    @POST("customer/jwt/register/")
    fun register(@Header("OTP") otp: String, @Body params: RequestBody): Call<JvtCredentials>

    //password restore 1 step
    @POST("mobile/otp/send/")
    fun verifyPhonePassRestore(@Body params: RequestBody): Call<String>

    //password restore 3 step
    @Headers("Content-Type: application/json")
    @POST("mobile/password/reset/")
    fun passwordReset(@Header("OTP") otp: String, @Body params: RequestBody): Call<String>


    //register/password restore 2 step
    @POST("mobile/otp/verify/")
    fun verifyOpt(@Body params: RequestBody): Call<String>


    @Headers("Content-Type: application/json")
    @POST("mobile/jwt/login/")
    fun login(@Header("role") role: String, @Body params: RequestBody): Call<JvtCredentials>

    @Headers("Content-Type: application/json")
    @POST("jwt/refresh/")
    fun refreshToken(@Body params: RequestBody): Call<JvtCredentials>


    @Multipart
    @POST("media/photo/")
    fun postPhoto(@Part file: MultipartBody.Part): Call<Photos>

    @Headers("Content-Type: application/json", "accept: application/json")
    @POST("orders/")
    fun postOrder(@Body params: RequestBody): Call<Order>

    @Headers("Content-Type: application/json")
    @GET("orders/")
    fun getOrders(@Header("Authorization") token: String, @Query("page_size") pageSize: Int): Call<OrdersApi>

    @GET("orders/{id}")
    fun getOrderById(@Path("id") id: Int): Call<Order>


    @POST("orders/{id}/cancel/")
    fun cancelOrder(@Path("id") id: Int, @Query("pay") pay: Boolean): Call<CancelOrder>

    @POST("orders/{id}/confirm/")
    fun confirmOrder(@Path("id") id: Int): Call<ConfirmOrder>


    @GET("profile/")
    fun getProfile(): Call<Profile>

    @POST("jwt/logout/")
    fun logout(@Body params: RequestBody): Call<Void>

    @POST("payments/add-card/temporary-order/")
    fun createBinding(): Call<CreateBindingResponse>

    @GET ("payments/cards/")
    fun getAllBindings(): Call<GetBindingsResponse>

    @POST("payments/add-card/check-creation-binding/")
    fun verifyBinding(@Body params: RequestBody): Call<VerifyBindingResponse>

}