package com.entexy.elevator.data.api.payment.binding.create.response

import com.google.gson.annotations.SerializedName

data class CreateBindingResponse(
    @SerializedName("order_id") val orderId: String,
    @SerializedName("form_url") val formUrl: String
)
