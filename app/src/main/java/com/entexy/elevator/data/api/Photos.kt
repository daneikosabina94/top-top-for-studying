package com.entexy.elevator.data.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Photos(
    @SerializedName("uuid") @Expose
    val imageUuid: String = "",
    @SerializedName("file") @Expose
    val imageUrl: String = ""
)