package com.entexy.elevator.data.api.telConfirm

import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiTelephoneConfirmContract {
    suspend fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String)
}