package com.entexy.elevator.data.api.myOrders

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class OneOrderShortInfo(

    @SerializedName("id")
    @Expose
    var id: Int = -1,
    @SerializedName("title")
    @Expose
    var title: String? = "",
    @SerializedName("address")
    @Expose
    var address: String? = "",
    @SerializedName("predicted_movers_count")
    @Expose
    var predictedMoversCount: Int = -1,
    @SerializedName("weight")
    @Expose
    var weight: String? = "",
    @SerializedName("dimensions")
    @Expose
    var dimensions: String? = "",
    @SerializedName("price")
    @Expose
    var price: Int = -1,
    @SerializedName("created_at")
    @Expose
    var created_at: String? = "",
    @SerializedName("status")
    @Expose
    var status: String? = ""

)
