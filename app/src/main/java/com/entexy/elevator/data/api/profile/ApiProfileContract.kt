package com.entexy.elevator.data.api.profile

import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiProfileContract {

    suspend fun getProfile(sharedPrefsUtils: SharedPrefsUtils): Profile
    suspend fun logout(sharedPrefsUtils: SharedPrefsUtils)
}