package com.entexy.elevator.data.api.payment.binding.create

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.api.payment.binding.create.response.CreateBindingResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.google.android.material.circularreveal.CircularRevealHelper.Delegate

class CreateBindingDataSource {
    fun createBinding(sharedPrefsUtils: SharedPrefsUtils): CreateBindingResponse {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.createBinding()?.execute()
        if (response?.code() == 200) return response.body()!!
        throw Exception()
    }
}