package com.entexy.elevator.data.repo

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.orderRegistration.ApiOrderRegistrationContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.OrderRegistrationDomainContact
import kotlinx.coroutines.flow.flow

class OrderRegistrationDataImpl(private val apiOrderRegistrationContract: ApiOrderRegistrationContract) : OrderRegistrationDomainContact {

    override suspend fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String, address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int, payment_card: String
    ) = flow {
        emit(OutcomeData.Progress)
        try {
            apiOrderRegistrationContract.postOrder(
                sharedPrefsUtils = sharedPrefsUtils,
                photosByteArrayList = photosByteArrayList,
                city = city,
                address = address,
                district = district,
                address_latitude = address_latitude,
                address_longitude = address_longitude,
                entrance = entrance,
                floor = floor,
                is_there_an_elevator = is_there_an_elevator,
                title = title,
                items_count = items_count,
                lifts_count = lifts_count,
                comment = comment,
                bring_to_floor = bring_to_floor,
                dimensions = dimensions,
                weight = weight,
                price = price,
                predicted_movers_count = predicted_movers_count, payment_card = payment_card
            )
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    override suspend fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = flow {
        emit(OutcomeData.Progress)
        try {
            emit(OutcomeData.Success(apiOrderRegistrationContract.getOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId)))
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    override suspend fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = flow {
        emit(OutcomeData.Progress)
        try {
            apiOrderRegistrationContract.confirmOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    override suspend fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean) = flow {
        emit(OutcomeData.Progress)
        try {
            apiOrderRegistrationContract.cancelOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId, isNeededPayment)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }


}