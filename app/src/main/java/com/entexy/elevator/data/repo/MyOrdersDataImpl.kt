package com.entexy.elevator.data.repo

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.myOrders.ApiMyOrdersContract
import com.entexy.elevator.data.api.myOrders.OneOrderShortInfo
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.MyOrdersDomainContact
import com.entexy.elevator.domain.models.OneOrderShortInfoDomain
import kotlinx.coroutines.flow.flow

class MyOrdersDataImpl(private val apiMyOrdersContract: ApiMyOrdersContract) : MyOrdersDomainContact {

    override suspend fun getOrders(sharedPrefsUtils: SharedPrefsUtils) = flow {
        emit(OutcomeData.Progress)
        try {
            val result = apiMyOrdersContract.getOrders(sharedPrefsUtils = sharedPrefsUtils)
            emit(OutcomeData.Success(apiToDomainMapper(result.results)))
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    private fun apiToDomainMapper(apiList: MutableList<OneOrderShortInfo>): MutableList<OneOrderShortInfoDomain> {
        val domainList = mutableListOf<OneOrderShortInfoDomain>()
        apiList.forEach {
            domainList.add(
                OneOrderShortInfoDomain(
                    id = it.id, title = it.title, address = it.address, predictedMoversCount = it.predictedMoversCount,
                    weight = it.weight, dimensions = it.dimensions, price = it.price, created_at = it.created_at, status = it.status
                )
            )
        }
        return domainList
    }

}