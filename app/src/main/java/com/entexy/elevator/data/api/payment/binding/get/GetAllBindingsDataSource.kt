package com.entexy.elevator.data.api.payment.binding.get

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.api.payment.binding.get.response.GetBindingsResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils

class GetAllBindingsDataSource {
    fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils): GetBindingsResponse {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.getAllBindings()?.execute()
        if (response?.code() == 200) return response.body()!!
        throw Exception("Error getting cards")
    }
}