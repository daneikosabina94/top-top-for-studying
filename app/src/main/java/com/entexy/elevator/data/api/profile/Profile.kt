package com.entexy.elevator.data.api.profile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Profile(
    @SerializedName("id") @Expose
    var id: String = "",
    @SerializedName("first_name") @Expose
    var firstName: String = "",
    @SerializedName("middle_name") @Expose
    var middleName: String = "",
    @SerializedName("last_name") @Expose
    var lastName: String = "",
    @SerializedName("phone_number") @Expose
    var phoneNumber: String = "",
    @SerializedName("email") @Expose
    var email: String = "",
    @SerializedName("city") @Expose
    var city: String = ""

)
