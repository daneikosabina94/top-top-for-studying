package com.entexy.elevator.data.api.myOrders

import com.google.gson.annotations.SerializedName

data class CancelOrder(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("cancelled_at")
    var cancelled_at: String? = null,
    @SerializedName("status")
    var status: String? = null

)