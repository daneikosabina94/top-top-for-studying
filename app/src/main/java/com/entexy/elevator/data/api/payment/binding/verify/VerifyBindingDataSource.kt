package com.entexy.elevator.data.api.payment.binding.verify

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.api.payment.binding.verify.response.VerifyBindingResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class VerifyBindingDataSource {
    fun verifyBinding(sharedPrefsUtils: SharedPrefsUtils): VerifyBindingResponse {
        val orderId = sharedPrefsUtils.getOrderId()
        val response = RestApiClient
            .getClientWithAccessToken(sharedPrefsUtils)
            ?.verifyBinding(createRequestBody("order_id" to orderId!!))?.execute()
        if (response?.code() == 200) return response.body()!!
        throw Exception()
    }

    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
}