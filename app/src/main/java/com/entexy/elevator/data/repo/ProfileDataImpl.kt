package com.entexy.elevator.data.repo

import android.util.Log
import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.profile.ApiProfileContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.ProfileDomainContact
import kotlinx.coroutines.flow.flow

class ProfileDataImpl(private val apiProfileContract: ApiProfileContract) : ProfileDomainContact {

    override suspend fun getProfile(sharedPrefsUtils: SharedPrefsUtils) = flow {
        emit(OutcomeData.Progress)
        try {
            emit(OutcomeData.Success(apiProfileContract.getProfile(sharedPrefsUtils = sharedPrefsUtils)))
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    override suspend fun logout(sharedPrefsUtils: SharedPrefsUtils) = flow {
        emit(OutcomeData.Progress)
        try {
            apiProfileContract.logout(sharedPrefsUtils = sharedPrefsUtils)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            Log.d("Check this", "${e.stackTraceToString()}")
            emit(OutcomeData.Failure(e))
        }
    }

}