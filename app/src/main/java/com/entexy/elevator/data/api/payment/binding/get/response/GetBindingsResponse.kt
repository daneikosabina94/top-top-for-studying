package com.entexy.elevator.data.api.payment.binding.get.response

import com.google.gson.annotations.SerializedName

data class GetBindingsResponse(
    @SerializedName("results") val result: List<BindingItem>
)