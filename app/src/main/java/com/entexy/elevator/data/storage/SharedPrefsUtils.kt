package com.entexy.elevator.data.storage

import android.content.Context

class SharedPrefsUtils(private val context: Context) {

    private val PREFS_NAME = "prefs"
    private val FIRST_START = "first_start"
    private val ACCESS_TOKEN_KEY = "access_token_key"
    private val REFRESH_TOKEN_KEY = "refresh_token_key"
    private val REMEMBER = "remember"
    private val USER_NAME = "user_name"
    private val USER_PHONE = "user_phone"
    private val ORDER_ID = "order_id"
    private val CURRENT_BINDING_ID = "current_binding_id"
    private val TEL = "tel"

    fun setFirstStart(isFirstStart: Boolean) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(FIRST_START, isFirstStart).apply()
    }

    fun getFirstStart() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getBoolean(FIRST_START, true)

    fun setOrderId(orderId: String) =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(ORDER_ID, orderId).apply()

    fun getOrderId() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(ORDER_ID, "")

    fun getCurrentBindingId() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(CURRENT_BINDING_ID, "")

    fun setCurrentBindingId(id: String) =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(CURRENT_BINDING_ID, id).apply()


    fun setAccessToken(tokenText: String) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(ACCESS_TOKEN_KEY, tokenText).apply()
    }

    fun getAccessToken() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(ACCESS_TOKEN_KEY, "") ?: ""


    fun setRefreshToken(tokenText: String) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(REFRESH_TOKEN_KEY, tokenText).apply()
    }

    fun getRefreshToken() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(REFRESH_TOKEN_KEY, "") ?: ""


    fun setRemember(remember: Boolean) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putBoolean(REMEMBER, remember).apply()
    }

    fun getRemember() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getBoolean(REMEMBER, false)

    fun setUserName(s: String) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(USER_NAME, s).apply()
    }

    fun getUserName() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(USER_NAME, "") ?: ""

    fun setUserPhone(s: String) {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().putString(USER_PHONE, s).apply()
    }

    fun getUserPhone() = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(USER_PHONE, "") ?: ""

}