package com.entexy.elevator.data.api.telConfirm

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ApiTelephoneConfirmImpl : ApiTelephoneConfirmContract {

    override suspend fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String) {
        val response =
            RestApiClient.getClient(sharedPrefsUtils)?.verifyOpt(createRequestBody("otp" to otp, "phone_number" to phone_number))?.execute()
        if (response?.code() != 200)
            throw Exception(response?.errorBody()?.string())
    }


    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

}