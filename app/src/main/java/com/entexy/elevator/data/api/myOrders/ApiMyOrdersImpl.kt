package com.entexy.elevator.data.api.myOrders

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils

class ApiMyOrdersImpl : ApiMyOrdersContract {
    override suspend fun getOrders(sharedPrefsUtils: SharedPrefsUtils): OrdersApi {
        val response =
            RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.getOrders("Bearer ${sharedPrefsUtils.getAccessToken()}", 1000)
                ?.execute()
        if (response?.code() == 200)
            return response.body()!!
        throw Exception(response?.errorBody()?.string())
    }

}