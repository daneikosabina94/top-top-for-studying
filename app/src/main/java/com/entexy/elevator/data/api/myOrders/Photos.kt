package com.entexy.elevator.data.api.myOrders

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Photos(

    @SerializedName("uuid")
    @Expose
    var uuid: String? = null,
    @SerializedName("file")
    @Expose
    var file: String? = null

)