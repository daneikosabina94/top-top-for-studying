package com.entexy.elevator.data.api.registration

import com.entexy.elevator.data.api.JvtCredentials
import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiRegistrationContract {

    suspend fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ): JvtCredentials

    suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String)


}