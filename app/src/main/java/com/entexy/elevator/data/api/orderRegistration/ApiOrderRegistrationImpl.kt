package com.entexy.elevator.data.api.orderRegistration

import com.entexy.elevator.data.api.Order
import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.CancelOrderException
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.util.UUID

class ApiOrderRegistrationImpl : ApiOrderRegistrationContract {
    override suspend fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String,
        address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int,
        payment_card: String
    ) {
        val arrayOfImagesUUID = photosByteArrayList.map { bytes ->
            val requestBody = RequestBody.create("image/jpg".toMediaTypeOrNull(), bytes)
            val filePart = MultipartBody.Part.createFormData("file", "${UUID.randomUUID()}.jpg", requestBody)
            RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.postPhoto(filePart)?.execute()?.body()?.imageUuid
        }
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.postOrder(
            createRequestBody(
                "city" to city,
                "address" to address,
                "district" to district,
                "address_latitude" to address_latitude,
                "address_longitude" to address_longitude,
                "entrance" to entrance,
                "floor" to floor,
                "is_there_an_elevator" to is_there_an_elevator,
                "title" to title,
                "items_count" to items_count,
                "lifts_count" to lifts_count,
                "comment" to comment,
                "bring_to_floor" to bring_to_floor,
                "dimensions" to dimensions,
                "weight" to weight,
                "photos_ids" to arrayOfImagesUUID,
                "price" to price,
                "predicted_movers_count" to predicted_movers_count,
                "payment_card" to payment_card
            )
        )?.execute()
        if (response?.code() != 201)
            throw Exception(response?.errorBody()?.string())
    }

    override suspend fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int): Order {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.getOrderById(orderId)?.execute()
        if (response?.code() == 200)
            return response.body()!!
        throw Exception(response?.errorBody()?.string())
    }


    override suspend fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.confirmOrder(orderId)?.execute()
        if (response?.code() != 200)
            throw Exception(response?.errorBody()?.string())
    }


    override suspend fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean) {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.cancelOrder(orderId, isNeededPayment)
            ?.execute()
        if (response?.code() != 200)
            if (response?.code() == 400) throw CancelOrderException() else throw Exception(response?.errorBody()?.string())
    }


    private fun createRequestBody(vararg params: Pair<String, Any>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

}