package com.entexy.elevator.data.api.payment.binding

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.payment.binding.create.CreateBindingDataSource
import com.entexy.elevator.data.api.payment.binding.create.response.CreateBindingResponse
import com.entexy.elevator.data.api.payment.binding.get.GetAllBindingsDataSource
import com.entexy.elevator.data.api.payment.binding.get.response.GetBindingsResponse
import com.entexy.elevator.data.api.payment.binding.verify.VerifyBindingDataSource
import com.entexy.elevator.data.api.payment.binding.verify.response.VerifyBindingResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.PaymentBindingContact
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class PaymentBindingContactImpl(
    private val createBindingDataSource: CreateBindingDataSource,
    private val verifyBindingDataSource: VerifyBindingDataSource,
    private val getAllBindingsDataSource: GetAllBindingsDataSource
) : PaymentBindingContact {
    override suspend fun createBinding(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<CreateBindingResponse>> = callbackFlow {
        trySend(OutcomeData.Progress)
        try {
            val response = createBindingDataSource.createBinding(sharedPrefsUtils)
            sharedPrefsUtils.setOrderId(response.orderId)
            trySend(OutcomeData.Success(response))
        } catch (e: Exception) {
            trySend(OutcomeData.Failure(e))
        }
        awaitClose()
    }

    override suspend fun verifyBinding(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<VerifyBindingResponse>> = callbackFlow {
        trySend(OutcomeData.Progress)
        try {
            val response = verifyBindingDataSource.verifyBinding(sharedPrefsUtils)
            sharedPrefsUtils.setCurrentBindingId(response.bindingId)
            trySend(OutcomeData.Success(response))
        } catch (e: Exception) {
            trySend(OutcomeData.Failure(e))
        }
        awaitClose()
    }

    override suspend fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<GetBindingsResponse>> =
        callbackFlow {
            trySend(OutcomeData.Progress)
            try {
                trySend(OutcomeData.Success(getAllBindingsDataSource.getAllBindings(sharedPrefsUtils)))
            } catch (e: Exception) {
                trySend(OutcomeData.Failure(e))
            }
            awaitClose()
        }
}