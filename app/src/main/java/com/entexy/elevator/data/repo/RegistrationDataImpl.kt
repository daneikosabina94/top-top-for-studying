package com.entexy.elevator.data.repo

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.registration.ApiRegistrationContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.RegistrationDomainContact
import kotlinx.coroutines.flow.flow

class RegistrationDataImpl(private val apiRegistrationContract: ApiRegistrationContract) : RegistrationDomainContact {

    override suspend fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = flow {
        emit(OutcomeData.Progress)
        try {
            val result = apiRegistrationContract.register(
                sharedPrefsUtils = sharedPrefsUtils,
                first_name = first_name,
                phone_number = phone_number,
                password1 = password1,
                password2 = password2,
                otp = otp
            )
            emit(OutcomeData.Success(mutableMapOf(result.access to result.refresh)))
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }

    }

    override suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = flow {
        emit(OutcomeData.Progress)
        try {
            apiRegistrationContract.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }


}