package com.entexy.elevator.data.api.apiUtils

sealed class OutcomeData<out T> {
    class Success<out R>(val data: R) : OutcomeData<R>()
    object Progress : OutcomeData<Nothing>()
    object Empty : OutcomeData<Nothing>()
    class Failure(val e: Exception) : OutcomeData<Nothing>()
}