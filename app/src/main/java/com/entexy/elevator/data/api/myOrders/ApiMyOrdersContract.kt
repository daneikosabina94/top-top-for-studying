package com.entexy.elevator.data.api.myOrders

import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiMyOrdersContract {

    suspend fun getOrders(sharedPrefsUtils: SharedPrefsUtils): OrdersApi
}