package com.entexy.elevator.data.api.passwordRestore

import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiPasswordRestoreContract {

    suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String)
    suspend fun passReset(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, password1: String, password2: String, otp: String)

}