package com.entexy.elevator.data.repo

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.passwordRestore.ApiPasswordRestoreContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.PasswordRestoreDomainContact
import kotlinx.coroutines.flow.flow

class PasswordRestoreDataImpl(private val apiPasswordRestoreContract: ApiPasswordRestoreContract) : PasswordRestoreDomainContact {

    override suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = flow {
        emit(OutcomeData.Progress)
        try {
            apiPasswordRestoreContract.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

    override suspend fun passReset(
        sharedPrefsUtils: SharedPrefsUtils,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = flow {
        emit(OutcomeData.Progress)
        try {
            apiPasswordRestoreContract.passReset(
                sharedPrefsUtils = sharedPrefsUtils,
                phone_number = phone_number,
                password1 = password1,
                password2 = password2,
                otp = otp
            )
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }


}