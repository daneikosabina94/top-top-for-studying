package com.entexy.elevator.data.api.apiUtils

import android.util.Log
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.presentation.activity.MainActivity
import okhttp3.Authenticator
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class JwtAuthenticator(private val sharedPrefsUtils: SharedPrefsUtils) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val refreshToken = sharedPrefsUtils.getRefreshToken()
        val logging = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Log.d("!!!", "Called error $message")
                println("34ss JwtAuthenticator message =  " + message)
            }
        })
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(RestApiClient.BASE_URL)
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val api = retrofit.create(Api::class.java)

        // Refresh your access_token using a synchronous api request
        val responseTokens = api.refreshToken(createRequestBody("refresh" to refreshToken)).execute();
        println("34ss JwtAuthenticator newAccessToken =  " + responseTokens.body()?.access)
        println("34ss JwtAuthenticator newRefreshToken =  " + responseTokens.body()?.refresh)
        val refreshTokenNew = responseTokens.body()?.refresh ?: ""
        val accessTokenNew = responseTokens.body()?.access ?: ""
        sharedPrefsUtils.setRefreshToken(refreshTokenNew)
        sharedPrefsUtils.setAccessToken(accessTokenNew)
        Log.d("Check this", "${responseTokens.code()}")
        if (responseTokens.code() == 400) MainActivity.logoutCallback?.logout()
        // Add new header to rejected request and retry it
        return response.request.newBuilder()
            .header("Authorization", "Bearer ${responseTokens.body()?.access}")
            .build();
    }

    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
}