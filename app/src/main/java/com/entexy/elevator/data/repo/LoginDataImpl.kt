package com.entexy.elevator.data.repo

import android.util.Log
import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.login.ApiLoginContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.LoginDomainContact
import kotlinx.coroutines.flow.flow

class LoginDataImpl(private val apiLoginContract: ApiLoginContract) : LoginDomainContact {

    override suspend fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String) = flow {
        emit(OutcomeData.Progress)
        try {
            val response = apiLoginContract.login(sharedPrefsUtils = sharedPrefsUtils, tel = tel, password = password)
            Log.d("Login response", "$response")
            sharedPrefsUtils.setAccessToken(response.access)
            sharedPrefsUtils.setRefreshToken(response.refresh)
            emit(OutcomeData.Success(mutableMapOf(response.access to response.refresh)))
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

}