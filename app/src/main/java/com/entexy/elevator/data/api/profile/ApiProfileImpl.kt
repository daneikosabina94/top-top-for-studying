package com.entexy.elevator.data.api.profile

import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ApiProfileImpl : ApiProfileContract {

    override suspend fun getProfile(sharedPrefsUtils: SharedPrefsUtils): Profile {
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.getProfile()?.execute()
        if (response?.code() == 200)
            return response.body()!!
        throw Exception(response?.errorBody()?.string())
    }

    override suspend fun logout(sharedPrefsUtils: SharedPrefsUtils) {
        val refreshToken = sharedPrefsUtils.getRefreshToken()
        val response = RestApiClient.getClientWithAccessToken(sharedPrefsUtils)?.logout(createRequestBody("refresh" to refreshToken))
            ?.execute()
        if (response?.code() != 200)
            throw Exception(response?.errorBody()?.string())
    }

    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
}
