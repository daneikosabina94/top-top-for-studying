package com.entexy.elevator.data.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Photo(

    @SerializedName("uuid" ) @Expose var uuid : String? = null,
    @SerializedName("file" ) @Expose var imageUrl : String? = null


)
