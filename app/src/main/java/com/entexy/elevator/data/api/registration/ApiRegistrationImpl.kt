package com.entexy.elevator.data.api.registration

import com.entexy.elevator.data.api.JvtCredentials
import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ApiRegistrationImpl : ApiRegistrationContract {

    override suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) {
        val response = RestApiClient.getClient(sharedPrefsUtils)
            ?.verifyRegistrationPhone("customer", createRequestBody("phone_number" to phone_number))?.execute()
        if (response?.code() != 200)
            throw Exception(response?.errorBody()?.string())
    }

    override suspend fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ): JvtCredentials {
        val response = RestApiClient.getClient(sharedPrefsUtils)?.register(
            otp,
            createRequestBody(
                "first_name" to first_name,
                "phone_number" to phone_number,
                "password1" to password1,
                "password2" to password2
            )
        )?.execute()
        if (response?.code() == 201)
            return response.body()!!
        throw Exception(response?.errorBody()?.string())
    }


    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())


}