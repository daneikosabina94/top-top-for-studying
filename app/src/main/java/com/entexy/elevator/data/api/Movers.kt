package com.entexy.elevator.data.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Movers(

    @SerializedName("first_name"  ) @Expose var firstName  : String? = null,
    @SerializedName("middle_name" ) @Expose var middleName : String? = null,
    @SerializedName("last_name"   ) @Expose var lastName   : String? = null

)
