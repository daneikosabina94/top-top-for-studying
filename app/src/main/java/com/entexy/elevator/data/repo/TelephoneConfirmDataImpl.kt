package com.entexy.elevator.data.repo

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.telConfirm.ApiTelephoneConfirmContract
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.interfacesToData.TelephoneConfirmDomainContact
import kotlinx.coroutines.flow.flow

class TelephoneConfirmDataImpl(private val apiTelephoneConfirmContract: ApiTelephoneConfirmContract) : TelephoneConfirmDomainContact {

    override suspend fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String) = flow {
        emit(OutcomeData.Progress)
        try {
            apiTelephoneConfirmContract.verifyOtp(sharedPrefsUtils = sharedPrefsUtils, otp = otp, phone_number = phone_number)
            emit(OutcomeData.Empty)
        } catch (e: Exception) {
            emit(OutcomeData.Failure(e))
        }
    }

}