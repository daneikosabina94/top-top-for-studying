package com.entexy.elevator.data.api

import com.google.gson.annotations.SerializedName

data class JvtCredentials(
    @SerializedName("refresh")
    val refresh: String = "",
    @SerializedName("access")
    val access: String = ""
)