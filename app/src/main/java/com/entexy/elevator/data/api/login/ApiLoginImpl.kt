package com.entexy.elevator.data.api.login

import com.entexy.elevator.data.api.JvtCredentials
import com.entexy.elevator.data.api.apiUtils.RestApiClient
import com.entexy.elevator.data.storage.SharedPrefsUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ApiLoginImpl : ApiLoginContract {

    override fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String): JvtCredentials {
        val response =
            RestApiClient.getClient(sharedPrefsUtils)?.login("customer", createRequestBody("phone_number" to tel, "password" to password))
                ?.execute()
        if (response?.code() == 200)
            return response.body()!!
        throw Exception(response?.errorBody()?.string())
    }

    private fun createRequestBody(vararg params: Pair<String, String>) =
        JSONObject(mapOf(*params)).toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

}