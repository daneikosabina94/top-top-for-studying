package com.entexy.elevator.data.api.myOrders

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class OrdersApi(
    @SerializedName("count")
    @Expose
    var count: Int = -1,
    @SerializedName("next")
    @Expose
    var next: String = "",
    @SerializedName("previous")
    @Expose
    var previous: String = "",
    @SerializedName("results")
    @Expose
    var results: ArrayList<OneOrderShortInfo> = arrayListOf()

)