package com.entexy.elevator.data.api.apiUtils

import android.content.Intent
import android.os.Handler
import android.widget.Toast
import com.entexy.elevator.App
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.presentation.activity.MainActivity
import com.facebook.stetho.okhttp3.StethoInterceptor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


object RestApiClient {
    //val BASE_URL = "http://192.168.31.249:8888/api/v1/"
    val BASE_URL = "https://elevator.entexy.com/api/v1/"

    fun getClientWithAccessToken(sharedPrefsUtils: SharedPrefsUtils): Api? {
        val logging = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                if (message.isEmpty()) return
                makeErrorToast(message = message, methodName = "getClientWithAccessToken", sharedPrefsUtils = sharedPrefsUtils)
            }
        })
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .connectTimeout(3, TimeUnit.SECONDS)
            .authenticator(JwtAuthenticator(sharedPrefsUtils))
            .addInterceptor(logging)
            .addNetworkInterceptor(StethoInterceptor())
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(Api::class.java)
    }


    fun getClient(sharedPrefsUtils: SharedPrefsUtils): Api? {
        val logging = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                if (message.isEmpty() || message.contains("https://elevator.entexy.com/api/v1/mobile/password/reset/")) return
                makeErrorToast(message = message, methodName = "getClient", sharedPrefsUtils = sharedPrefsUtils)
            }
        })
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(Api::class.java)
    }


    private fun makeErrorToast(message: String, methodName: String, sharedPrefsUtils: SharedPrefsUtils) {
        CoroutineScope(Dispatchers.Main).launch {
            println("34ss RestApiClient $methodName = $message")
            if (
                (message.contains("detail")
                        || message.contains("password")
                        || message.contains("Enter a valid phone number"))
                && (!message.contains("phone_number"))

            ) Toast.makeText(
                App.appContext, message
                    .replace("\"detail\":".toRegex(), "")
                    .replace("\"password\":".toRegex(), "")
                    .replace("\"phone_number\":".toRegex(), "")
                    .replace("\\{".toRegex(), "")
                    .replace("\\}".toRegex(), "")
                    .replace("\\[".toRegex(), "")
                    .replace("\\]".toRegex(), "")
                    .replace("\"".toRegex(), ""), Toast.LENGTH_LONG
            ).show()

            if (message.contains("уже зарегистрирован")) {
                Toast.makeText(App.appContext, "Такой номер телефона уже зарегистрирован в системе", Toast.LENGTH_LONG).show()
            }

            if (message.contains("Введите валидный номер телефона")) {
                Toast.makeText(App.appContext, "Введите валидный номер телефона", Toast.LENGTH_LONG).show()
            }

            if (message.contains("Пользователь не найден")) {
                println("34ss RestApiClient $methodName = Пользователь не найден")
                Toast.makeText(App.appContext, "Пользователь не найден", Toast.LENGTH_LONG).show()
                Handler().postDelayed({
                    sharedPrefsUtils.setAccessToken("")
                    val context = App.appContext
                    val intent = Intent(context, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    context.startActivity(intent)
                }, 2000)
            }


        }
    }

}