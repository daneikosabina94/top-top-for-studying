package com.entexy.elevator.data.api.orderRegistration

import com.entexy.elevator.data.api.Order
import com.entexy.elevator.data.storage.SharedPrefsUtils

interface ApiOrderRegistrationContract {

    suspend fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String, address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int, payment_card: String
    )

    suspend fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int): Order

    suspend fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean)

    suspend fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int)

}