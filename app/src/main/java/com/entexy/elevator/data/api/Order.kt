package com.entexy.elevator.data.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Order(



    @SerializedName("id"                   ) @Expose var id                : Int?              = null,
    @SerializedName("address"              ) @Expose var address           : String?           = null,
    @SerializedName("entrance"             ) @Expose var entrance          : Int?              = null,
    @SerializedName("floor"                ) @Expose var floor             : Int?              = null,
    @SerializedName("is_there_an_elevator" ) @Expose var isThereAnElevator : Boolean?          = null,
    @SerializedName("title"                ) @Expose var title             : String?           = null,
    @SerializedName("items_count"          ) @Expose var itemsCount        : Int?              = null,
    @SerializedName("lifts_count"          ) @Expose var liftsCount        : Int?              = null,
    @SerializedName("comment"              ) @Expose var comment           : String?           = null,
    @SerializedName("dimensions"           ) @Expose var dimensions        : String?           = null,
    @SerializedName("weight"               ) @Expose var weight            : String?           = null,
    @SerializedName("photos"               ) @Expose var photos            : ArrayList<Photos> = arrayListOf(),
    @SerializedName("brigadier"            ) @Expose var brigadier         : Brigadier?        = Brigadier(),
    @SerializedName("status"               ) @Expose var status            : String?           = null,
    @SerializedName("price"                ) @Expose var price             : Int?              = null,
    @SerializedName("payment_card"         ) @Expose var paymentCard       : String?           = null,
    @SerializedName("bring_to_floor"       ) @Expose var bringToFloor      : Boolean?          = null,
    @SerializedName("recipient"            ) @Expose var recipient         : String?           = null


)
