package com.entexy.myapplication.di

import com.entexy.elevator.data.api.login.ApiLoginContract
import com.entexy.elevator.data.api.login.ApiLoginImpl
import com.entexy.elevator.data.api.myOrders.ApiMyOrdersContract
import com.entexy.elevator.data.api.myOrders.ApiMyOrdersImpl
import com.entexy.elevator.data.api.orderRegistration.ApiOrderRegistrationContract
import com.entexy.elevator.data.api.orderRegistration.ApiOrderRegistrationImpl
import com.entexy.elevator.data.api.passwordRestore.ApiPasswordRestoreContract
import com.entexy.elevator.data.api.passwordRestore.ApiPasswordRestoreImpl
import com.entexy.elevator.data.api.payment.binding.PaymentBindingContactImpl
import com.entexy.elevator.data.api.payment.binding.create.CreateBindingDataSource
import com.entexy.elevator.data.api.payment.binding.get.GetAllBindingsDataSource
import com.entexy.elevator.data.api.payment.binding.verify.VerifyBindingDataSource
import com.entexy.elevator.data.api.profile.ApiProfileContract
import com.entexy.elevator.data.api.profile.ApiProfileImpl
import com.entexy.elevator.data.api.registration.ApiRegistrationContract
import com.entexy.elevator.data.api.registration.ApiRegistrationImpl
import com.entexy.elevator.data.api.telConfirm.ApiTelephoneConfirmContract
import com.entexy.elevator.data.api.telConfirm.ApiTelephoneConfirmImpl
import com.entexy.elevator.data.repo.*
import com.entexy.elevator.domain.interfacesToData.*
import org.koin.dsl.module


val dataModule = module {
    //GetOrders
    factory<ApiMyOrdersContract> { ApiMyOrdersImpl() }
    factory<MyOrdersDomainContact> { MyOrdersDataImpl(apiMyOrdersContract = get()) }

    //LoginViewModel
    factory<ApiLoginContract> { ApiLoginImpl() }
    factory<LoginDomainContact> { LoginDataImpl(apiLoginContract = get()) }

    //OrderRegistrationViewModel
    factory<ApiOrderRegistrationContract> { ApiOrderRegistrationImpl() }
    factory<OrderRegistrationDomainContact> { OrderRegistrationDataImpl(apiOrderRegistrationContract = get()) }

    //PasswordRestoreViewModel
    factory<ApiPasswordRestoreContract> { ApiPasswordRestoreImpl() }
    factory<PasswordRestoreDomainContact> { PasswordRestoreDataImpl(apiPasswordRestoreContract = get()) }

    //ProfileViewModel
    factory<ApiProfileContract> { ApiProfileImpl() }
    factory<ProfileDomainContact> { ProfileDataImpl(apiProfileContract = get()) }

    //RegistrationViewModel
    factory<ApiRegistrationContract> { ApiRegistrationImpl() }
    factory<RegistrationDomainContact> { RegistrationDataImpl(apiRegistrationContract = get()) }

    //paymentBinding
    factory { CreateBindingDataSource() }

    factory { GetAllBindingsDataSource() }

    factory { VerifyBindingDataSource() }

    //paymentImpl
    factory<PaymentBindingContact> { PaymentBindingContactImpl(get(), get(), get()) }

    //TelephoneConfirmViewModel
    factory<ApiTelephoneConfirmContract> { ApiTelephoneConfirmImpl() }
    factory<TelephoneConfirmDomainContact> { TelephoneConfirmDataImpl(apiTelephoneConfirmContract = get()) }

}