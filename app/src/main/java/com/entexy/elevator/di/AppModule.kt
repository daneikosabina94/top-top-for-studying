package com.entexy.myapplication.di

import com.entexy.elevator.presentation.activity.MainViewModel
import com.entexy.elevator.presentation.viewmodels.GetOrdersViewModel
import com.entexy.elevator.presentation.viewmodels.LoginViewModel
import com.entexy.elevator.presentation.viewmodels.OrderRegistrationViewModel
import com.entexy.elevator.presentation.viewmodels.PasswordRestoreViewModel
import com.entexy.elevator.presentation.viewmodels.ProfileViewModel
import com.entexy.elevator.presentation.viewmodels.RegistrationViewModel
import com.entexy.elevator.presentation.viewmodels.TelephoneConfirmViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    //GetOrders
    viewModel<GetOrdersViewModel> { GetOrdersViewModel(myOrdersUseCase = get(), get()) }

    //LoginViewModel
    viewModel<LoginViewModel> { LoginViewModel(loginUseCase = get()) }

    viewModel<MainViewModel> { MainViewModel(get()) }
    //OrderRegistrationViewModel
    viewModel<OrderRegistrationViewModel> {
        OrderRegistrationViewModel(
            orderRegistrationUseCase = get(),
            createBindingUseCase = get(),
            verifyBindingUseCase = get(),
            getAllBindingsUseCase = get()
        )
    }

    //PasswordRestoreViewModel
    viewModel<PasswordRestoreViewModel> { PasswordRestoreViewModel(passwordRestoreUseCase = get()) }

    //ProfileViewModel
    viewModel<ProfileViewModel> { ProfileViewModel(profileUseCase = get()) }

    //RegistrationViewModel
    viewModel<RegistrationViewModel> { RegistrationViewModel(registrationUseCase = get()) }

    //TelephoneConfirmViewModel
    viewModel<TelephoneConfirmViewModel> { TelephoneConfirmViewModel(telephoneConfirmUseCase = get()) }

}