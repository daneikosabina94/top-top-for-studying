package com.entexy.myapplication.di

import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.uscases.CreateBindingUseCase
import com.entexy.elevator.domain.uscases.GetAllBindingsUseCase
import com.entexy.elevator.domain.uscases.GetOrdersUseCase
import com.entexy.elevator.domain.uscases.LoginUseCase
import com.entexy.elevator.domain.uscases.OrderRegistrationUseCase
import com.entexy.elevator.domain.uscases.PasswordRestoreUseCase
import com.entexy.elevator.domain.uscases.ProfileUseCase
import com.entexy.elevator.domain.uscases.RegistrationUseCase
import com.entexy.elevator.domain.uscases.TelephoneConfirmUseCase
import com.entexy.elevator.domain.uscases.VerifyBindingUseCase
import org.koin.dsl.module

val domainModule = module {
    //GetOrders
    factory<GetOrdersUseCase> { GetOrdersUseCase(myOrdersDomainContact = get(), get()) }

    //LoginViewModel
    factory<LoginUseCase> { LoginUseCase(loginDomainContact = get(), get()) }

    //OrderRegistrationViewModel
    factory<OrderRegistrationUseCase> { OrderRegistrationUseCase(orderRegistrationDomainContact = get(), get()) }

    //PasswordRestoreViewModel
    factory<PasswordRestoreUseCase> { PasswordRestoreUseCase(passwordRestoreDomainContact = get(), get()) }

    //ProfileViewModel
    factory<ProfileUseCase> { ProfileUseCase(profileDomainContact = get(), get()) }

    //RegistrationViewModel
    factory<RegistrationUseCase> { RegistrationUseCase(registrationDomainContact = get(), get()) }

    //TelephoneConfirmViewModel
    factory<TelephoneConfirmUseCase> { TelephoneConfirmUseCase(telephoneConfirmDomainContact = get(), get()) }

    factory { CreateBindingUseCase(get(), get()) }

    factory { GetAllBindingsUseCase(get(), get()) }

    factory { VerifyBindingUseCase(get(), get()) }

    factory<ExceptionHandler>{ ExceptionHandler.Base() }

}