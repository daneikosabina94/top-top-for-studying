package com.entexy.elevator

import android.app.Application
import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.entexy.elevator.presentation.RefreshOrdersListener
import com.entexy.myapplication.di.appModule
import com.entexy.myapplication.di.dataModule
import com.entexy.myapplication.di.domainModule
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.newInitializerBuilder(this).let { builder ->
            builder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
            builder.enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
            Stetho.initialize(builder.build())
        }
        appContext = applicationContext

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(listOf(appModule, domainModule, dataModule))
        }

        circularProgressDrawable = CircularProgressDrawable(appContext)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f

    }


    companion object {
        lateinit var appContext: Context

        lateinit var circularProgressDrawable: CircularProgressDrawable

        fun startAnim(view: View){
            val rotateAnimation = RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            rotateAnimation.duration = 400
            view.startAnimation(rotateAnimation)
        }

        lateinit var refreshOrdersListener: RefreshOrdersListener
    }
}