package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.ProfileDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class ProfileUseCase(private val profileDomainContact: ProfileDomainContact, private val exceptionHandler: ExceptionHandler) {

    suspend fun getProfile(sharedPrefsUtils: SharedPrefsUtils) = flow {
        profileDomainContact.getProfile(sharedPrefsUtils = sharedPrefsUtils).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Success -> emit(OutcomeUi.Success(it.data))
                else -> {}
            }
        }
    }

    suspend fun logout(sharedPrefsUtils: SharedPrefsUtils) = flow {
        profileDomainContact.logout(sharedPrefsUtils = sharedPrefsUtils).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }

}