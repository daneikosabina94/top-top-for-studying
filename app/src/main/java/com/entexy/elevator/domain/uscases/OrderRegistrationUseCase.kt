package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.OrderRegistrationDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class OrderRegistrationUseCase(
    private val orderRegistrationDomainContact: OrderRegistrationDomainContact,
    private val exceptionHandler: ExceptionHandler
) {

    suspend fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String, address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int, payment_card: String
    ) = flow {
        orderRegistrationDomainContact.postOrder(
            sharedPrefsUtils = sharedPrefsUtils,
            photosByteArrayList = photosByteArrayList,
            city = city,
            address = address,
            district = district,
            address_latitude = address_latitude,
            address_longitude = address_longitude,
            entrance = entrance,
            floor = floor,
            is_there_an_elevator = is_there_an_elevator,
            title = title,
            items_count = items_count,
            lifts_count = lifts_count,
            comment = comment,
            bring_to_floor = bring_to_floor,
            dimensions = dimensions,
            weight = weight,
            price = price,
            predicted_movers_count = predicted_movers_count, payment_card = payment_card
        ).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }

    suspend fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = flow {
        orderRegistrationDomainContact.getOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Success -> emit(OutcomeUi.Success(it.data))
                else -> {}
            }
        }
    }

    suspend fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int) = flow {
        orderRegistrationDomainContact.confirmOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }

    suspend fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean) = flow {
        orderRegistrationDomainContact.cancelOrder(sharedPrefsUtils = sharedPrefsUtils, orderId = orderId, isNeededPayment).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }
}