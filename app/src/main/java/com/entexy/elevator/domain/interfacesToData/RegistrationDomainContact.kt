package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface RegistrationDomainContact {
    suspend fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ): Flow<OutcomeData<MutableMap<String, String>>>

    suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String): Flow<OutcomeData<Unit>>


}