package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.payment.binding.create.response.CreateBindingResponse
import com.entexy.elevator.data.api.payment.binding.get.response.GetBindingsResponse
import com.entexy.elevator.data.api.payment.binding.verify.response.VerifyBindingResponse
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface PaymentBindingContact {
    suspend fun createBinding(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<CreateBindingResponse>>
    suspend fun verifyBinding(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<VerifyBindingResponse>>
    suspend fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<GetBindingsResponse>>
}