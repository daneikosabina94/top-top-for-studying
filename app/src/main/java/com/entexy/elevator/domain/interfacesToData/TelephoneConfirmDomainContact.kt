package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface TelephoneConfirmDomainContact {
    suspend fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String): Flow<OutcomeData<Unit>>
}