package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.PaymentBindingContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.callbackFlow

class VerifyBindingUseCase(private val paymentBindingContact: PaymentBindingContact, private val exceptionHandler: ExceptionHandler) {
    suspend fun verifyBinding(sharedPrefsUtils: SharedPrefsUtils) = callbackFlow {
        paymentBindingContact.verifyBinding(sharedPrefsUtils).collect {
            when (it) {
                is OutcomeData.Success -> trySend(OutcomeUi.Success(it.data))
                is OutcomeData.Failure -> trySend(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Progress -> trySend(OutcomeUi.Progress)
                else -> {}
            }
        }
    }
}