package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.PasswordRestoreDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class PasswordRestoreUseCase(
    private val passwordRestoreDomainContact: PasswordRestoreDomainContact,
    private val exceptionHandler: ExceptionHandler
) {

    suspend fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = flow {
        passwordRestoreDomainContact.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }

    suspend fun passReset(
        sharedPrefsUtils: SharedPrefsUtils,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = flow {
        passwordRestoreDomainContact.passReset(
            sharedPrefsUtils = sharedPrefsUtils,
            phone_number = phone_number,
            password1 = password1,
            password2 = password2,
            otp = otp
        ).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }
}