package com.entexy.elevator.domain.core

import android.accounts.NetworkErrorException
import com.entexy.elevator.R
import java.net.ConnectException
import java.net.UnknownHostException

interface ExceptionHandler {
    fun handle(e: Exception, customHandel: () -> (Int)): Int

    fun handle(e: Exception): Int

    class Base : ExceptionHandler {
        override fun handle(e: Exception, customHandel: () -> (Int)): Int {
            val customResId = customHandel()
            if (customResId != 0)
                return customResId
            return handle(e)
        }

        override fun handle(e: Exception) = when (e) {
            is NetworkErrorException -> R.string.error_internet
            is ConnectException -> R.string.error_server
            is UnknownHostException -> R.string.error_internet
            is CancelOrderException -> -2
            else -> -1
        }
    }
}