package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.models.OneOrderShortInfoDomain
import kotlinx.coroutines.flow.Flow

interface MyOrdersDomainContact {
    suspend fun getOrders(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<MutableList<OneOrderShortInfoDomain>>>
}