package com.entexy.elevator.domain.uscases

import android.util.Log
import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.LoginDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class LoginUseCase(private val loginDomainContact: LoginDomainContact, private val exceptionHandler: ExceptionHandler) {

    suspend fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String) = flow {
        loginDomainContact.login(sharedPrefsUtils = sharedPrefsUtils, tel = tel, password = password).collect {
            Log.d("Login response", "$it")
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Success -> emit(OutcomeUi.Success(it.data))
                else -> {}
            }
        }
    }

}