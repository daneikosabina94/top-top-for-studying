package com.entexy.elevator.domain.models

import com.entexy.elevator.data.api.Order
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class OneOrderShortInfoDomain(
    @SerializedName("id")
    var id: Int = -1,
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("address")
    var address: String? = "",
    @SerializedName("predicted_movers_count")
    var predictedMoversCount: Int = -1,
    @SerializedName("weight")
    var weight: String? = "",
    @SerializedName("dimensions")
    var dimensions: String? = "",
    @SerializedName("price")
    var price: Int = -1,
    @SerializedName("created_at")
    var created_at: String? = "",
    @SerializedName("status")
    var status: String? = ""
)