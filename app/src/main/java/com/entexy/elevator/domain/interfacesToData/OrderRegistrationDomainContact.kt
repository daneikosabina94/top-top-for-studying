package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.Order
import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface OrderRegistrationDomainContact {

    suspend fun postOrder(
        sharedPrefsUtils: SharedPrefsUtils,
        photosByteArrayList: MutableList<ByteArray>,
        city: String, address: String,
        district: String,
        address_latitude: Double,
        address_longitude: Double,
        entrance: Int,
        floor: Int,
        is_there_an_elevator: Boolean,
        title: String,
        items_count: Int,
        lifts_count: Int,
        comment: String,
        bring_to_floor: Boolean,
        dimensions: String,
        weight: String,
        price: Int,
        predicted_movers_count: Int, payment_card: String
    ): Flow<OutcomeData<Unit>>

    suspend fun getOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int): Flow<OutcomeData<Order>>

    suspend fun cancelOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int, isNeededPayment: Boolean): Flow<OutcomeData<Unit>>

    suspend fun confirmOrder(sharedPrefsUtils: SharedPrefsUtils, orderId: Int): Flow<OutcomeData<Unit>>

}