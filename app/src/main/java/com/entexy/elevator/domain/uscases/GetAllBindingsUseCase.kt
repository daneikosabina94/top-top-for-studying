package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.PaymentBindingContact
import com.entexy.elevator.presentation.fragments.order.adapters.item.CardItem
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.callbackFlow

class GetAllBindingsUseCase(private val paymentBindingContact: PaymentBindingContact, private val exceptionHandler: ExceptionHandler) {
    suspend fun getAllBindings(sharedPrefsUtils: SharedPrefsUtils) = callbackFlow {
        paymentBindingContact.getAllBindings(sharedPrefsUtils).collect {
            when (it) {
                is OutcomeData.Progress -> trySend(OutcomeUi.Progress)
                is OutcomeData.Failure -> trySend(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Success -> {
                    val cardList = it.data.result.map { item ->
                        CardItem(
                            item.cardNumber, item.paymentSystem, item.bindingId,
                            item.bindingId == sharedPrefsUtils.getCurrentBindingId()
                        )
                    }
                    trySend(OutcomeUi.Success(cardList))
                }

                else -> {}
            }
        }
    }
}