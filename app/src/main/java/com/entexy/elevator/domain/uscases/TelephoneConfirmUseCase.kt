package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.TelephoneConfirmDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class TelephoneConfirmUseCase(
    private val telephoneConfirmDomainContact: TelephoneConfirmDomainContact,
    private val exceptionHandler: ExceptionHandler
) {

    suspend fun verifyOtp(sharedPrefsUtils: SharedPrefsUtils, otp: String, phone_number: String) = flow {
        telephoneConfirmDomainContact.verifyOtp(sharedPrefsUtils = sharedPrefsUtils, otp = otp, phone_number = phone_number).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }

}