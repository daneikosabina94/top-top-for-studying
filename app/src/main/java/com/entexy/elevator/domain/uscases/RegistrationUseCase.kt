package com.entexy.elevator.domain.uscases

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import com.entexy.elevator.domain.core.ExceptionHandler
import com.entexy.elevator.domain.interfacesToData.RegistrationDomainContact
import com.entexy.elevator.presentation.helpers.OutcomeUi
import kotlinx.coroutines.flow.flow

class RegistrationUseCase(
    private val registrationDomainContact: RegistrationDomainContact,
    private val exceptionHandler: ExceptionHandler
) {

    fun register(
        sharedPrefsUtils: SharedPrefsUtils,
        first_name: String,
        phone_number: String,
        password1: String,
        password2: String,
        otp: String
    ) = flow {
        registrationDomainContact.register(
            sharedPrefsUtils = sharedPrefsUtils,
            first_name = first_name,
            phone_number = phone_number,
            password1 = password1,
            password2 = password2,
            otp = otp
        ).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                is OutcomeData.Success -> emit(OutcomeUi.Success(it.data))
                else -> {}
            }
        }
    }

    fun verifyPhone(sharedPrefsUtils: SharedPrefsUtils, phone_number: String, from: String) = flow {
        registrationDomainContact.verifyPhone(sharedPrefsUtils = sharedPrefsUtils, phone_number = phone_number, from = from).collect {
            when (it) {
                is OutcomeData.Progress -> emit(OutcomeUi.Progress)
                is OutcomeData.Failure -> emit(OutcomeUi.Failure(exceptionHandler.handle(it.e)))
                else -> emit(OutcomeUi.Empty)
            }
        }
    }


}