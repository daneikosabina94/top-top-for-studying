package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface LoginDomainContact {
    suspend fun login(sharedPrefsUtils: SharedPrefsUtils, tel: String, password: String): Flow<OutcomeData<MutableMap<String, String>>>
}