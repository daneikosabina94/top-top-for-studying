package com.entexy.elevator.domain.interfacesToData

import com.entexy.elevator.data.api.apiUtils.OutcomeData
import com.entexy.elevator.data.api.profile.Profile
import com.entexy.elevator.data.storage.SharedPrefsUtils
import kotlinx.coroutines.flow.Flow

interface ProfileDomainContact {

    suspend fun getProfile(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<Profile>>
    suspend fun logout(sharedPrefsUtils: SharedPrefsUtils): Flow<OutcomeData<Unit>>

}